<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bandara extends Model
{
    protected $table ='tbl_bandara';
    protected $primaryKey ='bandara_id';
    protected $fillable =['bandara_name','bandara_desc'];
    public $timestamps = false;
}
