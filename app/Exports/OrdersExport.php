<?php

namespace App\Exports;

use App\Order;
use App\VReport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class OrdersExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // varible form and to 
    public function __construct(String $from = null , String $to = null, String $gran = null, $terminal = null)
    {
        $this->from = $from;
        $this->to   = $to;
        $this->gran   = $gran;
        $this->terminal   = $terminal;
    }
    //function select data from database 
    public function collection()
    {
        try {
            // $data = array(
            //     'from' => $this->from,
            //     'to' => $this->to,
            //     'gran' => $this->gran,
            //     'terminal' => $this->terminal,
            // );
            // dd($data);
            if($this->gran == 'daily')
            {
                $data = VReport::
                where('date_order','>=',$this->from)
                ->where('date_order','<=', $this->to)
                ->where('terminal', 'LIKE', $this->terminal)
                ->get(); 
            }
            if($this->gran == 'weekly')
            {
                $year_now = date("Y");
                $data = VReport::
                where('terminal', 'LIKE', $this->terminal)
                ->where('tahun', $year_now)
                ->get();
            }
            if($this->gran == 'monthly')
            {
                $month_now = date("m");
                $year_now = date("Y");
                $data = VReport::
                where('terminal', 'LIKE', $this->terminal)
                ->where('tahun', $year_now)
                ->get();
            }
            return $data;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function map($antrian): array
    {
        try {
            // This example will return 3 rows.
            // First row will have 2 column, the next 2 will have 1 column
            return [
                [
                    $antrian->bandara,
                    $antrian->terminal,
                    $antrian->date_order,
                    $antrian->time_order,
                    $antrian->kendaraan_kirim_ke_pickup_point,            
                    $antrian->kendaraan_tiba_di_pickup_point,  //arrived          
                    $antrian->closed_antrian,
                    $antrian->waktu_tunggu_customer,
                    $antrian->nomor_antrian,
                    $antrian->kode_otp,
                    $antrian->pilihan_rute,
                    $antrian->status_pesanan,
                    $antrian->nomor_kendaraan,
                    
                ]
            ];
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function registerEvents(): array
    {
        try {
            return [
                AfterSheet::class    => function(AfterSheet $event) {
                    $cellRange = 'A1:W1'; // All headers
                    $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
                    // $event->cell->setValignment('center');
                    // $event->sheet->getStyle('A1:W1')->applyFromArray(array(
                    //     'fill' => array(
                    //         // 'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    //         'color' => array('rgb' => '9DE292')
                    //     )
                    // ));
                },
            ];
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

     //function header in excel
     public function headings(): array
     {
         return [
             'Bandara',
             'Terminal',
             'Tanggal Order',
             'Waktu Order',
             'Waktu Kendaraan dikirim ke pickup point',
             'Waktu Kendaraan tiba di pickup point',
             'Waktu Antrian di closed',
             'Waktu Tunggu Customer',
             'Nomor Antrian',
             'Kode OTP',
             'Pilihan Rute',
             'Status Pesanan',
             'Nomor Kendaraan',
        ];
    }
}
