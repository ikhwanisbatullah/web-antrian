<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroundStaff extends Model
{
    
    protected $table ='v_ground_staff';
}
