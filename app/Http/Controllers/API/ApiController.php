<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Lokasi;

class ApiController extends Controller
{
    public function dashboard_daily(Request $request)
    {
        try {
            $categories = DB::select("SELECT date(created_at) AS tanggal FROM tbl_orders WHERE date(created_at) = date(NOW())  AND location_id = 1 GROUP BY date(created_at)");
            foreach($categories as $cat){
                $cancel = DB::select("SELECT COUNT(a.id) AS total
                FROM `tbl_log_order` a 
                WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'cancel'  AND a.location_id = 1");

                $waiting = DB::select("SELECT COUNT(a.order_id) AS total
                FROM `tbl_orders` a 
                WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'waiting'  AND a.location_id = 1");

                $otw = DB::select("SELECT COUNT(a.order_id) AS total
                FROM `tbl_orders` a 
                WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'on the way'  AND a.location_id = 1");
        
                $closed = DB::select("SELECT COUNT(a.id) AS total
                FROM `tbl_log_order` a 
                WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'closed'  AND a.location_id = 1");

                $totals = DB::select("SELECT COUNT(a.id) AS total
                FROM `tbl_log_order` a
                WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal'  AND a.location_id = 1");

                $data[] = array(
                    'tanggal' => $cat->tanggal,
                    'total'  => $totals[0]->total+$waiting[0]->total+$otw[0]->total,
                    'closed'  => $closed[0]->total,
                    'cancel'  => $cancel[0]->total,
                    'waiting'  => $waiting[0]->total,
                    'otw'  => $otw[0]->total,
                );
            }   
            if(empty($data)){
                $data[] = array(
                    'tanggal' => 0,
                    'total'  => 0,
                    'closed'  => 0,
                    'cancel'  => 0,
                    'waiting'  => 0,
                    'otw'  => 0,
                );
            }
            return response()->json([
                'status'  => 200,
                'msg'     => 'succes',
                'data'    => $data

            ],200);   
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        
    }
    public function grafik(Request $request){
        try {
            // request
            $date_from = $request->date_from;
            $terminal = $request->terminal;
            $granularity = $request->granularity;
            if($date_from == 'undefined'){
                $date_from = null;
            }
            $date_to = $request->date_to;
            if($date_to == 'undefined'){
                $date_to = null;
            }
            if($terminal == 'undefined'){
                $terminal = null;
            }
            if($granularity == 'undefined'){
                $granularity = 'daily';
            }
            $terminals = "%".$terminal."%";
            // request

            // validation
            if($granularity == 'daily')
            {
                if(!empty($date_to) && $date_to !== 'undefined'){
                    // dd($request->all());
                    $categories = DB::select("SELECT date(created_at) AS tanggal FROM tbl_log_order WHERE date(created_at) >= '$date_from' AND date(created_at) <=  '$date_to' AND location_id = 1 GROUP BY date(created_at)");
                    
                    foreach($categories as $cat){
                        $cancel = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'cancel' AND c.location_name LIKE '$terminals'");
            
                        $closed = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'closed' AND c.location_name LIKE '$terminals'");
                        
    
                        $totals = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND c.location_name LIKE '$terminals'");
        
                        $waiting = DB::select("SELECT COUNT(a.order_id) AS total
                        FROM `tbl_orders` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'waiting' AND c.location_name LIKE '$terminals'");
        
                        $otw = DB::select("SELECT COUNT(a.order_id) AS total
                        FROM `tbl_orders` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'on the way' AND c.location_name LIKE '$terminals'");
                        
                        // $waiting = DB::select("SELECT COUNT(a.order_id) AS total
                        // FROM `tbl_orders` a 
                        // WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'waiting'  AND a.location_id = 1");
        
                        // $otw = DB::select("SELECT COUNT(a.order_id) AS total
                        // FROM `tbl_orders` a 
                        // WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'on the way'  AND a.location_id = 1");
                
                        // AVG
                        // $avg = DB::select("SELECT DATE_FORMAT(created_at, '%Y-%m-%d') AS tanggal, COUNT(created_at) AS jumlah, DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(update_at,created_at)))), '%H:%i:%s') AS avg FROM tbl_orders WHERE DATE_FORMAT(created_at, '%Y-%m-%d') = '$cat->tanggal'  AND status_pesanan = 'closed'  AND location_id =1 GROUP BY DAY(created_at);
                        // ");
                        
                        // $averages[] = [
                        //     'tanggal' => $cat->tanggal,
                        //     'avg_waiting_time'   => $avg[0]->avg
                        // ];  
                        // AVG        
    
                        $data[] = array(
                            'tanggal' => $cat->tanggal,
                            // 'total'  => $totals[0]->total+$waiting[0]->total+$otw[0]->total,
                            'total'  => $totals[0]->total,
                            'closed'  => $closed[0]->total,
                            // 'cancel'  => $cancel[0]->total+$waiting[0]->total+$otw[0]->total,
                            'cancel'  => $cancel[0]->total,
                            'waiting'  => $waiting[0]->total,
                            'otw'  => $otw[0]->total,
                        );
                    }   
                    if(empty($data)){
                        // $averages[] = [
                        //     'tanggal' => 0,
                        //     'avg_waiting_time'   => 0
                        // ];  
                        
                        $data[] = array(
                            'tanggal' => 0,
                            'total'  => 0,
                            'closed'  => 0,
                            'cancel'  => 0,
                            'waiting'  => 0,
                            'otw'  => 0,
                        );
                    }
                    return response()->json([
                        'status'  => 200,
                        'msg'     => 'succes',
                        'data'    => $data,
                        // 'avgs' => $averages
            
                    ],200);          
                } else{
                    // where from date
                    // $categories = DB::select("SELECT date(created_at) AS tanggal FROM tbl_orders WHERE DATE_FORMAT(created_at, '%Y-%m-%d') = '$date_from' AND location_id = 1 GROUP BY date(created_at)");
                    $categories = DB::select("SELECT date(a.created_at) AS tanggal FROM tbl_log_order a  LEFT JOIN v_location b ON a.location_id = b.id  WHERE DATE_FORMAT(a.created_at, '%Y-%m-%d') = '$date_from' AND b.name LIKE '$terminals' GROUP BY date(a.created_at)
                    ");
                    foreach($categories as $cat){
                        $cancel = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'cancel' AND c.location_name LIKE '$terminals'");
            
                        $closed = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'closed' AND c.location_name LIKE '$terminals'");
                        
                        $totals = DB::select("SELECT COUNT(a.id) AS total
                        FROM `tbl_log_order` a 
                        LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND c.location_name LIKE '$terminals'");
        
                        // $waiting = DB::select("SELECT COUNT(a.id) AS total
                        // FROM `tbl_log_order` a 
                        // LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        // LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        // WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'waiting' AND a.location_id = 1");
        
                        // $otw = DB::select("SELECT COUNT(a.id) AS total
                        // FROM `tbl_log_order` a 
                        // LEFT JOIN tbl_bandara b ON a.bandara_id = b.bandara_id 
                        // LEFT JOIN tbl_location c ON a.location_id = c.location_id
                        // WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status = 'on the way' AND a.location_id = 1");
        
                        $waiting = DB::select("SELECT COUNT(a.order_id) AS total
                        FROM `tbl_orders` a 
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'waiting'  AND a.location_id = 1");
        
                        $otw = DB::select("SELECT COUNT(a.order_id) AS total
                        FROM `tbl_orders` a 
                        WHERE date_format(a.`created_at`,'%Y-%m-%d') = '$cat->tanggal' AND a.status_pesanan = 'on the way'  AND a.location_id = 1");
                
                        // AVG
                        // $avg = DB::select("SELECT DATE_FORMAT(created_at, '%Y-%m-%d') AS tanggal, COUNT(created_at) AS jumlah, DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(update_at,created_at)))), '%H:%i:%s') AS avg FROM tbl_orders WHERE DATE_FORMAT(created_at, '%Y-%m-%d') = '$cat->tanggal'  AND status_pesanan = 'closed'  AND location_id =1 GROUP BY DAY(created_at);
                        // ");
                        // $averages[] = [
                        //     'tanggal' => $cat->tanggal,
                        //     'avg_waiting_time'   => $avg[0]->avg
                        // ];  
                         // AVG   
        
                        $data[] = array(
                            'tanggal' => $cat->tanggal,
                            'total'  => $totals[0]->total,
                            // 'total'  => $totals[0]->total+$waiting[0]->total+$otw[0]->total,
                            'closed'  => $closed[0]->total,
                            'cancel'  => $cancel[0]->total,
                            'waiting'  => $waiting[0]->total,
                            'otw'  => $otw[0]->total,
                        );
                    }
                    if(empty($data)){
                        // $averages[] = [
                        //     'tanggal' => 0,
                        //     'avg_waiting_time'   => 0
                        // ];  
                        
                        $data[] = array(
                            'tanggal' => 0,
                            'total'  => 0,
                            'closed'  => 0,
                            'cancel'  => 0,
                            'waiting'  => 0,
                            'otw'  => 0,
                        );
                    }
                    return response()->json([
                        'status'  => 200,
                        'msg'     => 'succes',
                        'data'    => $data,
                        // 'avgs' => $averages,
        
                    ],200); 
                }
            }

            if($granularity == 'weekly')
            {
                $datas = DB::select("SELECT 
                WEEK(created_at) AS tanggal,
                created_at
                -- COUNT(created_at) AS total,
                -- DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS waiting
                FROM tbl_log_order 
                WHERE YEAR(created_at) = YEAR(NOW())
                GROUP BY WEEK(created_at);
                ");
                foreach($datas as $d){
                    // $totals = DB::select("SELECT 
                    // WEEK(created_at) AS week,
                    // created_at,
                    // COUNT(created_at) AS total,
                    // DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS waiting
                    // FROM tbl_log_order 
                    // WHERE 
                    // WEEK(created_at) = '$d->tanggal' AND
                    // YEAR(created_at) = YEAR(NOW())
                    // GROUP BY WEEK(created_at);
                    // ");

                    $totals = DB::select("SELECT 
                    WEEK(a.created_at) AS week,
                        COUNT(a.created_at) AS total,
                        DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting
                    FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE WEEK(created_at) = '$d->tanggal' AND
                    YEAR(created_at) = YEAR(NOW()) AND b.name LIKE '$terminals' GROUP BY WEEK(created_at)");
                    if(empty($totals)){
                        $totals = 0;
                    }else{
                        $totals = $totals[0]->total;
                    }

                    // $closed = DB::select("SELECT 
                    // WEEK(created_at) AS week,
                    // created_at,
                    // COUNT(created_at) AS total,
                    // DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS waiting
                    // FROM tbl_log_order 
                    // WHERE 
                    // status = 'closed' AND
                    // WEEK(created_at) = '$d->tanggal' AND
                    // YEAR(created_at) = YEAR(NOW())
                    // GROUP BY WEEK(created_at);
                    // ");

                    $closed = DB::select("SELECT 
                    WEEK(a.created_at) AS week,
                        COUNT(a.created_at) AS total,
                        DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting
                    FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE a.status = 'closed' AND WEEK(created_at) = '$d->tanggal' AND
                    YEAR(created_at) = YEAR(NOW()) AND b.name LIKE '$terminals' GROUP BY WEEK(created_at)");
                    if(empty($closed)){
                        $closed = 0;
                    }else{
                        $closed = $closed[0]->total;
                    }

                    // $cancel = DB::select("SELECT 
                    // WEEK(created_at) AS week,
                    // created_at,
                    // COUNT(created_at) AS total,
                    // DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS waiting
                    // FROM tbl_log_order 
                    // WHERE 
                    // status = 'cancel' AND
                    // WEEK(created_at) = '$d->tanggal' AND
                    // YEAR(created_at) = YEAR(NOW())
                    // GROUP BY WEEK(created_at);
                    // ");

                    $cancel = DB::select("SELECT 
                    WEEK(a.created_at) AS week,
                        COUNT(a.created_at) AS total,
                        DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting
                    FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE a.status = 'cancel' AND WEEK(created_at) = '$d->tanggal' AND
                    YEAR(created_at) = YEAR(NOW()) AND b.name LIKE '$terminals' GROUP BY WEEK(created_at)");

                    if(empty($cancel)){
                        $cancel = 0;
                    }else{
                        $cancel = $cancel[0]->total;
                    }
                    $data[] = array(
                        'tanggal' => "week ".$d->tanggal,
                        // 'total'  => $totals[0]->total+$waiting[0]->total+$otw[0]->total,
                        'total'  => $totals,
                        'closed'  => $closed,
                        // 'cancel'  => $cancel[0]->total+$waiting[0]->total+$otw[0]->total,
                        'cancel'  => $cancel,
                        // 'waiting'  => 0,
                        // 'otw'  => 0,
                    );
                }
                return response()->json([
                    'status'  => 200,
                    'msg'     => 'succes',
                    'data'    => $data,
        
                ],200); 
            }

            if($granularity == 'monthly')
            {
                $datas = DB::select("SELECT 
                MONTHNAME(created_at) AS tanggal
                -- COUNT(created_at) AS total, 
                -- DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS waiting 
                FROM tbl_log_order WHERE DATE_FORMAT(created_at, '%Y') = YEAR(NOW()) GROUP BY MONTH(created_at);
                ");
                foreach($datas as $d){
                    $totals = DB::select("SELECT 
                    MONTHNAME(a.created_at) AS tanggal,
                    COUNT(a.created_at) AS total, 
                    DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting 
                    FROM tbl_log_order a LEFT JOIN v_location b  ON a.location_id = b.id WHERE DATE_FORMAT(a.created_at, '%Y') = YEAR(NOW()) AND 
                    MONTHNAME(a.created_at) = '$d->tanggal' AND b.name LIKE '$terminals'
                    GROUP BY MONTH(a.created_at)
                    ");

                    // $totals = DB::select("SELECT 
                    // MONTHNAME(a.created_at) AS tanggal,
                    //     COUNT(a.created_at) AS total,
                    //     DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting
                    // FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE a.status = 'closed' AND MONTHNAME(a.created_at) = '$d->tanggal' AND
                    // YEAR(a.created_at) = YEAR(NOW()) AND b.name LIKE '$terminals' GROUP BY MONTH(a.created_at)");

                    if(empty($totals)){
                        $totals = 0;
                    }else{
                        $totals = $totals[0]->total;
                    }

                    $closed = DB::select("SELECT 
                    MONTHNAME(a.created_at) AS tanggal,
                    COUNT(a.created_at) AS total, 
                    DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting 
                    FROM tbl_log_order a LEFT JOIN v_location b  ON a.location_id = b.id WHERE DATE_FORMAT(a.created_at, '%Y') = YEAR(NOW()) AND 
                    MONTHNAME(a.created_at) = '$d->tanggal' AND b.name LIKE '$terminals' AND a.status = 'closed'
                    GROUP BY MONTH(a.created_at)
                    ");
                    if(empty($closed)){
                        $closed = 0;
                    }else{
                        $closed = $closed[0]->total;
                    }

                    $cancel = DB::select("SELECT 
                    MONTHNAME(a.created_at) AS tanggal,
                    COUNT(a.created_at) AS total, 
                    DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS waiting 
                    FROM tbl_log_order a LEFT JOIN v_location b  ON a.location_id = b.id WHERE DATE_FORMAT(a.created_at, '%Y') = YEAR(NOW()) AND 
                    MONTHNAME(a.created_at) = '$d->tanggal' AND b.name LIKE '$terminals' AND a.status = 'cancel'
                    GROUP BY MONTH(a.created_at)
                    ");
                    if(empty($cancel)){
                        $cancel = 0;
                    }else{
                        $cancel = $cancel[0]->total;
                    }
                    $data[] = array(
                        'tanggal' => $d->tanggal,
                        // 'total'  => $totals[0]->total+$waiting[0]->total+$otw[0]->total,
                        'total'  => $totals,
                        'closed'  => $closed,
                        // 'cancel'  => $cancel[0]->total+$waiting[0]->total+$otw[0]->total,
                        'cancel'  => $cancel,
                        // 'waiting'  => 0,
                        // 'otw'  => 0,
                    );
                }
                return response()->json([
                    'status'  => 200,
                    'msg'     => 'succes',
                    'data'    => $data,
        
                ],200); 
            }
            // validation
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
        
    }
    public function index(Request $request)
    {
        try {
            // request
            $date_from = $request->date_from;
            $terminal = $request->terminal;
            $granularity = $request->granularity;
            if($date_from == 'undefined'){
                $date_from = null;
            }
            $date_to = $request->date_to;
            if($date_to == 'undefined'){
                $date_to = null;
            }
            if($terminal == 'undefined'){
                $terminal = null;
            }
            if($granularity == 'undefined'){
                $granularity = 'daily';
            }
            $terminals = "%".$terminal."%";
            // request
        
            //validation
            if($granularity == 'daily')
            {
                if(!empty($date_to) && $date_to !== 'undefined'){
                    $query = DB::select("SELECT COUNT(a.id) AS total, b.nama_bandara, date(a.created_at) AS tanggal, b.name AS terminal, DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS avg
                     FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE date(a.created_at) >= '$date_from' AND date(a.created_at) <=  '$date_to' AND b.name LIKE '$terminals' GROUP BY b.name");
                }
                else{
                    
                    $query = DB::select("SELECT COUNT(a.id) AS total, b.nama_bandara, date(a.created_at) AS tanggal, b.name AS terminal, DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(updated_at,created_at)))), '%H:%i:%s') AS avg
                     FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE date(a.created_at) = '$date_from' AND b.name LIKE '$terminals' GROUP BY b.name");
                }
                
            }
            if($granularity == 'weekly')
            {
                $query = DB::select("SELECT 
                date(a.created_at), 
                b.nama_bandara,
                b.name AS terminal,
                WEEK(a.created_at) AS week,
                    COUNT(a.created_at) AS total,
                    DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS avg
                FROM tbl_log_order a LEFT JOIN v_location b ON a.location_id = b.id WHERE b.name LIKE '$terminals' GROUP BY b.name;
                "); 
                // dd($query);
            }
            if($granularity == 'monthly')
            {
                $query = DB::select("SELECT b.nama_bandara,
                b.name AS terminal, MONTHNAME(a.created_at) AS bulan, COUNT(a.created_at) AS total, DATE_FORMAT(SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(a.updated_at,a.created_at)))), '%H:%i:%s') AS avg FROM tbl_log_order a LEFT JOIN v_location b  ON a.location_id = b.id WHERE DATE_FORMAT(a.created_at, '%Y') = YEAR(NOW()) AND b.name LIKE '$terminals' GROUP BY 
                b.name, b.nama_bandara 
                -- MONTH(a.created_at);
                ");
            }
            // validation
        
            $draw = intval($request->draw);
            $start = intval($request->start);
            $length = intval($request->length);
            
            // collect data
            $data = [];
            $i=1;
            foreach($query as $r) {
                $data[] = array(
                    $r->nama_bandara,
                    $r->terminal,
                    $r->total,
                    $r->avg,
                );
            }
            // dd($data);
            $result = array(
                "draw" => $draw,
                "recordsTotal" => count($query),
                "recordsFiltered" => count($query),
                "data" => $data
            );
            return response()->json($result,200);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function getTerminal(Request $request)
    {
        try {
            $datas = Lokasi::where('bandara_id', $request->id)->get();
            return response()->json([
                'status'  => 200,
                'msg'     => 'succes',
                'data'    => $datas
            ],200);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getData(Request $request)
    {
        try {
            $rqid = $request->rqid;
            $start = $request->startDate;
            $end = $request->endDate;
            if($rqid == 'PAYCUBE-QMS')
            {
                if(!empty($start) && !empty($end))
                {
                    $datas = DB::select("SELECT * FROM v_report WHERE date_order >= '$start' AND date_order <=  '$end'");
                    
                }
                else if(!empty($start) && empty($end))
                {
                    $datas = DB::select("SELECT * FROM v_report WHERE date_order = '$start'");
                }
                else{
                    return response()->json([
                        'status'  => 400,
                        'msg'     => 'succes',
                        'data'    => 'masukan parameter startDate & endDate'
                    ],400);
                }
                return response()->json([
                    'status'  => 200,
                    'msg'     => 'succes',
                    'data'    => $datas
                ],200);
            }else{
                return response()->json([
                    'status'  => 400,
                    'msg'     => 'error',
                    'data'    => 'rqid not match',
                ],400);
            }
        } catch (\Exception $e) {
            // echo $e->getMessage();
            return response()->json([
                'status'  => 400,
                'msg'     => 'succes',
                'data'    => $e->getMessage()
            ],400);
        }
    }
}
