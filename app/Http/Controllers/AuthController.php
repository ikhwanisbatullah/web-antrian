<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('auths.login');
    }

    public function postlogin(Request $request)
    {
        if(Auth::attempt($request->only('email','password'), true)){
            $request->session()->regenerate();
            $role=auth()->user()->level_user;
            if($role== 'super-admin')
            {
                return redirect()->intended('/dashboard');
            }
            else if($role== 'administrator')
            {
                return redirect()->intended('/admin/dataAkun/akun');
            }
            else if($role== 'admin-dashboard')
            {
                return redirect()->intended('/dashboard');
            }
            else{
                return redirect('/dashboard');
            }

        }
        return redirect('/');
        // if(!Auth::check()) 
    
        // return "You can't access here!";
    
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function cek(){
        if(Auth::check())
            return "Access Granted!";
        else
            return "Access Denied!";

    }

}
