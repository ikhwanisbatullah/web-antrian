<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bandara;

class BandaraController extends Controller
{
    public function kategori_bandara(){
        try {
            $kategori_bandara = Bandara::all();
            $i = 0;
            $base_url = url('/');
            return view('bandara/index', compact('kategori_bandara', 'i', 'base_url'));
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function tambah_kategori_bandara(Request $request){
        try {
            Bandara::create($request->all());
            return redirect('/admin/dataBandara/kategoriBandara')->with('alert success', 'Data Bandara berhasil ditambahkan!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function ubah_kategori_bandara(Request $request)
    {
        try {
            $kategori_bandara = Bandara::findOrFail($request->bandara_id);
            $kategori_bandara->bandara_id = $request->bandara_id;
            $kategori_bandara->bandara_name = $request->bandara_name;
            $kategori_bandara->bandara_desc = $request->bandara_desc;
            $kategori_bandara->save();
            return redirect('/admin/dataBandara/kategoriBandara')->with('alert success', 'Data Bandara berhasil diubah!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hapus_kategori_bandara($bandara_id){
        try {
            $bandara = Bandara::findOrFail($bandara_id);
            $bandara->delete($bandara);
            return redirect('/admin/dataBandara/kategoriBandara')->with('alert danger', 'Data Bandara berhasil dihapus!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
