<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use App\Lokasi;
use App\Bandara;
use App\User;
use App\Order;
use DB;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(){
        
       
        // $order = \App\Order::all();
        // Menyiapkan data untuk chart
        // $cancel = [];
        // $data = [];
        // $status =[];
        // $monthly = [];
        // $order =DB::select(`status_pesanan`)
		// ->addSelect(DB::raw(`COUNT(status_pesanan) as cancel`))
		// ->from(`tbl_orders`)
		// ->groupBy(`status_pesanan`)
		// ->get();
        // $monthly =DB::select()->addSelect(JANUARY(created_at))->addSelect(DB::raw(`COUNT(created_at) as jumlah`))->from(`tbl_orders`)->where(`DATE_FORMAT(created_at, '%Y')`, `=`, 2020)->groupBy(`MONTH`)->get();
        
        // $cancel = DB::table('tbl_orders')->where('status_pesanan', cancel)->count();
        // foreach($order as $o){
        //     $status[] =$o->DB::select(`status_pesanan`)
        //     ->addSelect(DB::raw(`COUNT(status_pesanan) as cancel`))
        //     ->from(`tbl_orders`)
        //     ->groupBy(`status_pesanan`)
        //     ->get();;
        // }
        // dd($status);
        // dd($categories);
        // dd(json_encode($categories));

    return view('dashboard.index');
    // return view('Admin.dashboardAdmin');
    }
    public function export() 
    {
       // dd('apajsk');
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    public function orderReport()
{
    //INISIASI 30 HARI RANGE SAAT INI JIKA HALAMAN PERTAMA KALI DI-LOAD
    //KITA GUNAKAN STARTOFMONTH UNTUK MENGAMBIL TANGGAL 1
    $start = Carbon::now()->startOfMonth()->format('Y-m-d H:i:s');
    //DAN ENDOFMONTH UNTUK MENGAMBIL TANGGAL TERAKHIR DIBULAN YANG BERLAKU SAAT INI
    $end = Carbon::now()->endOfMonth()->format('Y-m-d H:i:s');

    //JIKA USER MELAKUKAN FILTER MANUAL, MAKA PARAMETER DATE AKAN TERISI
    if (request()->date != '') {
        //MAKA FORMATTING TANGGALNYA BERDASARKAN FILTER USER
        $date = explode(' - ' ,request()->date);
        $start = Carbon::parse($date[0])->format('Y-m-d') . ' 00:00:01';
        $end = Carbon::parse($date[1])->format('Y-m-d') . ' 23:59:59';
    }

    //BUAT QUERY KE DB MENGGUNAKAN WHEREBETWEEN DARI TANGGAL FILTER
    $orders = Order::with(['customer.district'])->whereBetween('created_at', [$start, $end])->get();
    //KEMUDIAN LOAD VIEW
    return view('report.order', compact('orders'));
}
}
