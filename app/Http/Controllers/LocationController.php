<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lokasi;
use App\Vlokasi;
use Auth;
use App\Bandara;
use DB;
use Illuminate\Support\Facades\Crypt;

class LocationController extends Controller
{
    public function tests()
    {
        return view('Admin/tests');
    }
    public function test($id)
    {
        $data_id = Crypt::decrypt($id);       
        dd($data_id);
    }
    public function lokasi(){
        try {
            $base_url = url('/');
            $sesion = Auth::user();
            if ($sesion->level_user == "administrator") { //admin bandara
                $locations = VLokasi::where('id_bandara', $sesion->bandara_id)->get();
                $bandara = Bandara::where('bandara_id', $sesion->bandara_id)->first();
            } else{ //admin
                $locations = VLokasi::all();
                $bandara = Bandara::all();
            }
            $i = 0;
            return view('location/index', compact('i', 'locations', 'bandara', 'base_url'));
        } catch (\Exception $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function tambah_lokasi(Request $request){
        //return dd($request);
        try {
            $lokasi = new Lokasi();
            $lokasi->location_name = $request->location_name;
            $lokasi->bandara_id = $request->bandara_id;
            $lokasi->location_desc='-';
            $lokasi->save();
            return redirect('/admin/dataLokasi/Lokasi')->with('alert success', 'Lokasi berhasil ditambahkan!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function hapus_lokasi($id){
        try {
            $lokasi = Lokasi::findOrFail($id);
            $lokasi = Lokasi::where('location_id', $id)->first();
            $lokasi->delete($lokasi);
            return redirect('/admin/dataLokasi/Lokasi')->with('alert danger', 'Lokasi berhasil dihapus!');
        } catch (\Exception $e) {
            $error = $e->getMessage();
            echo $error;
        }
    }

    public function ubah_lokasi(Request $request){
        try {
            $lokasi = Lokasi::findOrFail($request->id);
            $lokasi->location_name = $request->name;
            $lokasi->bandara_id = $request->bandara_id;
            $lokasi->save();
            return redirect('/admin/dataLokasi/Lokasi')->with('alert success', 'Lokasi berhasil diubah!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    // public function hapus_lokasi($id){
    //     $lokasi = Lokasi::findOrFail($id);
    //     $lokasi->delete($lokasi);

    //     return redirect('/admin/dataLokasi/Lokasi')->with('alert danger', 'Lokasi berhasil dihapus!');
    // }

    public function ubah_lokasi_terminal(Request $request){
       
        $lokasi = Lokasi::findOrFail($request->location_id);
        $lokasi->location_name = $request->location_name;
        $lokasi->save();
        return redirect('/admin/dataLokasi/Lokasi')->with('alert success', 'Lokasi berhasil diubah!');
    }
}
