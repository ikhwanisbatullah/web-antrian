<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\VReport;
use DB;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrdersExport;

class ReportController extends Controller
{
    //
    // public function index(){
    //     $order = Order::all();
    //     $i = 0;
    //     return view('Admin/report', compact('order', 'i'));
    // }
    public function index(Request $req)
    {
        $method = $req->method();

        if ($req->isMethod('post'))
        {
            $from = $req->input('from');
            $to   = $req->input('to');
            $i = 0;
            //dd($from);
           //($to);
            if ($req->has('search'))
            {
                // select search
                //dd($to);
                //dd($to."-".$from);
                
                $dateDummy = "2020-12-17 09:06:53";
                $dateFormat = date("Y-m-d", strtotime($dateDummy));

                $dateFormatFrom = date("Y-m-d", strtotime($from));
                $dateFormatTo = date("Y-m-d", strtotime($to));
                
                // $search = DB::select("SELECT * FROM tbl_orders WHERE date_format(created_at, '%Y-%m-%d') BETWEEN '$dateFormatFrom' AND '$dateFormatTo'");
                $search= VReport::where('date_order','>=',$dateFormatFrom)->where('date_order','<=', $dateFormatTo)->get(); 
                // var_dump($search);
                // dd($search);
                // die();
               // dd($search);
               //dd($data);
     
        
                //return DB::select("SELECT * FROM tbl_orders WHERE date_format(created_at, '%Y-%m-%d') BETWEEN '$dateFormatFrom' AND '$dateFormatTo'");
                // $data = VReport::where('date_order','>=',$dateFormatFrom)->where('date_order','<=', $dateFormatTo)->get();  
                // return $data;
               
                return view('Admin/report',['ViewsPage' => $search, 'from' => $from, 'to' => $to,'i'=>$i]);
                //return view('Admin/report', compact('ViewsPage', 'i'));
              // retrun view('import',compact('search','from','to'));
            } 
            elseif ($req->has('exportPDF'))
            {
                // select PDF
               // $PDFReport = DB::select("SELECT * FROM users WHERE date_format(email_verified_at, '%Y-%m-%d') BETWEEN '$from' AND '$to'");
                $PDFReport = DB::select("SELECT * FROM tbl_orders WHERE date_format(created_at, '%Y-%m-%d') BETWEEN '$from' AND '$to'");
                $pdf = PDF::loadView('PDF_report', ['PDFReport' => $PDFReport])->setPaper('a4', 'landscape');
                return $pdf->download('PDF-report.pdf');
            } 


                elseif($req->has('exportExcel'))
                        
                // select Excel
                return Excel::download(new OrdersExport($from, $to), 'gojek-antrian.xlsx');
            {
        } 
        }
            else
        {
            //select all
            $ViewsPage = DB::select('SELECT * FROM v_report');
            //$ViewsPage = DB::select('SELECT * FROM users');
           // dd($ViewsPage);
           // return view('import',['ViewsPage' => $ViewsPage]);
            $i = 0;
            return view('Admin/report', compact('ViewsPage', 'i'));
        }
    }

    public function export_chart(Request $request)
    {
        try {
            $from = $request->input('from');
            $to   = $request->input('to');
            $gran   = $request->input('gran');
            $terminal   = $request->input('terminal');
            $terminals = "%".$terminal."%";
            $i = 0;
            // $array = array(
            //     'from' => $from,
            //     'to' => $to,
            //     'gran' => $gran,
            //     't' => $terminals,
            // );
            // dd($array);
            return Excel::download(new OrdersExport($from, $to, $gran, $terminals), 'export_'.date("Ymdhis").'.xlsx');    
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
    }

    public function chart(Request $request)
    {
        try {
            return view('charts/index');
        } catch (\Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
}
