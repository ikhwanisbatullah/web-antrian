<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\User;
//use App\Cities;
use App\Bandara;
use App\Vlokasi;
use App\GroundStaff;
use DB;
use Auth;
use App\Http\Controllers\APIController as API;
use Illuminate\Support\Facades\Hash;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;


class UsersController extends Controller
{
    function encrypt($cleartext){
		//key length should be 16
		$key = "rb!nBwXv4C%Gr^84";
		
		//iv length should be 16
		$iv = "1234567812345678";
		
		//With the Java side, using 128 bits, 256-bit java end is not supported by default
		$Method = 'AES-128-CBC';
		
		$key = empty($key) ? $key : $key;
		
		$encrypted = openssl_encrypt($cleartext, $Method, $key, OPENSSL_RAW_DATA, $iv);
		
		return base64_encode($encrypted);
		
	}

    function decrypt($encrypted){
		//key length should be 16
		$key = "rb!nBwXv4C%Gr^84";
		
		//iv length should be 16
		$iv = "1234567812345678";
		
		//With the Java side, using 128 bits, 256-bit java end is not supported by default
		$Method = 'AES-128-CBC';
		
		$key = empty($key) ? $key : $key;
 
		$encrypted = base64_decode($encrypted);
 
		$decrypted = openssl_decrypt($encrypted, $Method, $key, OPENSSL_RAW_DATA, $iv);
 
		return trim($decrypted);
	}

    public function akun(){
        try {
            $base_url = url('/');
            $sesion = Auth::user();
            $i = 0;
            if ($sesion->level_user == "administrator") { //admin bandara
                $users = GroundStaff::where('bandara_id', $sesion->bandara_id)->get();
                $bandara = Bandara::where('bandara_id', $sesion->bandara_id)->first();
                $locations = DB::table('tbl_location')->where('bandara_id', $sesion->bandara_id)->get();
                return view('users/index', compact('i', 'users', 'bandara', 'locations', 'base_url'));
            } else{ //admin
                $users = GroundStaff::all();
                $bandara = Bandara::all();
                $locations = DB::table('tbl_location')->get();
                return view('users/index', compact('i', 'users', 'bandara', 'locations', 'base_url'));
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function hapus_akun($id){
        //dd($id);
        $data = DB::table('tbl_users')->where('user_id', $id)->delete();
        if($data){
            $profile = DB::table('tbl_profile')->where('user_id', $id)->delete();
            return redirect('/admin/dataAkun/akun')->with('alert danger', 'Akun berhasil dihapus!');
        }
        //dd($data);

    }

    public function tambah_akun(Request $request){
        // dd($request->location_id);
        try {
            $validator = Validator::make($request->all(), [
                'username'      => 'required|string|max:25|unique:tbl_users',
                'email'     => 'required|string|email|max:255|unique:tbl_users',
                // 'password'  => 'required|string|min:6|confirmed',      
            ]);
    
            if($validator->fails()){
                // return response()->json([
                //     'status'    => 400,
                //     'success'   => false, 
                //     'msg'       => $validator->errors()->toJson(),
                // ], 400);
                return redirect('/admin/dataAkun/akun')->with('alert danger', 'Terjadi Kesalahan!');
            }
            //insert ke tabel user
            $user = new User;
            $user->username = $request->username;
            // $user->password = Hash::make($request->password, [
            //     'rounds' => 12
            // ]);
            $user->password = $this->encrypt($request->password);
            $user->email = $request->email;
            $user->bandara_id = $request->bandara_id;
            $user->location_id=$request->location_id;
            $user->verify_email='yes';
            $user->level_login=$request->level_user;
            $user->user_online='online';
            $user->user_status='on'; 
            $user->remember_token = str_random(60);
            $user->save();

            //insert ke table  profile
            $profile = new Profile;
            $profile->user_id = $user->user_id;
            $profile->nama_lengkap = $request->nama_lengkap;
            $profile->no_hp = $request->no_hp;
            $profile->tempat_lahir = $request->tempat_lahir;
            $profile->tgl_lahir = $request->tgl_lahir;
            $profile->alamat_rumah = $request->alamat_rumah;
            $profile->jenis_kelamin = $request->jenis_kelamin;
            $profile->avatar = "avatar.png";
            $profile->save();
        
            $pesan = "Informasi Akun Aplikasi Antrian: ";
            $username = "Username : ".$request->username;
            $password = "Password : ".$request->password;

            return redirect('/admin/dataAkun/akun')->with('alert success', 'Data berhasil ditambahkan!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        
    }

    public function edit(Request $request)
    {
        try {
            // $data = DB::table('tbl_users')->where('user_id', $request->id)->first();
            $data = User::where('user_id',$request->id)->first();
            $data->username = $request->username;
            $data->password = $this->encrypt($request->password);
            $data->email = $request->email;
            $data->bandara_id = $request->bandara_id;
            $data->location_id = $request->location_id;
            $data->level_login = $request->level_login; //carpool/pickup
            $data->save();

            $profile = Profile::where('user_id', $request->id)->first();
            $profile->nama_lengkap = $request->nama_lengkap;
            $profile->no_hp = $request->no_hp;
            $profile->save();
            return redirect('/admin/dataAkun/akun')->with('alert success', 'Akun berhasil diubah!');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function ubah_akun_member(Request $request){

        // $this->validate($request, [
        //     'nomor_ktp=' => '|digits:16|numeric|regex:/^([1-9][0-9]+)/',
        //     'alamat_lengkap' => '|max:255',
        //     'thn_ijazah' => '|digits:4|numeric|regex:/^([1-9][0-9]+)/',
        //     'username' => '|max:191',
        //     'nomor_kontak' => '|max:15',
        //     'email' => '|max:30'
        // ]);


        return redirect('/admin/dataMember/akunMember')->with('alert success', 'Akun berhasil diubah!');
    }
    public function edit_akun($id)
    {
        $member = User::find($id);
        $profile = Profile::find($id);
        $kategori_bandara = Bandara::all();
        // $kota = Cities::all();
        return view('Admin/editAkun',compact('member', 'profile','kategori_bandara'));
    }
    public function update_akun(Request $request)
    {
        dd($request);
        $user = User::findOrFail($request->id);
        $user->username = $request->username;
        $user->password = Hash::make($request->password, [
            'rounds' => 12
        ]);
        $user->email = $request->email;
        $user->bandara_id = $request->bandara_id;
        $user->verify_email='yes';
        $user->level_user=$request->level_user;
        $user->location_id=$request->location_id;
        $user->user_online='online';
        $user->user_status='on'; 
        $user->remember_token = str_random(60);
        $user->save();

         //insert ke table  profile
        $profile = Profile::findOrFail($request->id);
        //$request->request->add(['user_id' =>$user->user_id ]);
        $profile->user_id = $user->user_id;
        $profile->nama_lengkap = $request->nama_lengkap;
        $profile->no_hp = $request->no_hp;
        $profile->tempat_lahir = $request->tempat_lahir;
        $profile->tgl_lahir = $request->tgl_lahir;
        $profile->alamat_rumah = $request->alamat_rumah;
        $profile->jenis_kelamin = $request->jenis_kelamin;
        $profile->save();
        //dd($member);
        return redirect('/admin/dataMember/akunMember')->with('alert success', 'Akun berhasil diubah!');
    }

    public function export() 
    {
       // dd('apajsk');
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
