<?php

namespace App\Http\Middleware;

use Closure;

class CheckLevelUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    ///titik 3 kali berfungsi supaya role nya banyak
    public function handle($request, Closure $next, ...$level_users)
    {
        // dd($request->user()->level_user);
        if(in_array($request->user()->level_user,$level_users)){
            return $next($request);
        }
        // return redirect('/');
        return "not access this page";

    }
}
