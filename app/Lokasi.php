<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table ='tbl_location';
    protected $primaryKey ='location_id';
    protected $fillable =['bandara_id','location_desc','location_name'];
    public $timestamps = false;
}
