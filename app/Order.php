<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table ='tbl_orders';
    protected $fillable =['nomor_pesanan','kode_pesanan','bandara_id','location_id','id_driver','status_pesanan','ganjil_genap','nomor_kendaraan'];
    public $timestamps = false;
}
