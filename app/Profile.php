<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table ='tbl_profile';
    protected $fillable =['nama_lengkap','no_hp','tempat_lahir','tgl_lahir','alamat_rumah','jenis_kelamin','avatar','user_id'];
    public $timestamps = false;
}
