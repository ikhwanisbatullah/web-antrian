<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vlokasi extends Model
{
    protected $table ='v_location';
    protected $fillable =['id','name','id_bandara','nama_bandara'];
    public $timestamps = false;
}
