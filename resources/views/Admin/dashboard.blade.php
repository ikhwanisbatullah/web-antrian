@extends('Admin/masterAdmin')

@section('judul_tab', 'Dashboard-Admin')
    
@section('active_menu_dashboard', 'active')

@section('css_after')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Daftar Bandara</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Data Bandara</a></li>
                            <li class="active">Bandara</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!-- Widgets  -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="col-lg-11">
                            <div id="graph"> </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('js_after')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script type="text/javascript">
    getBarReport();

    function getBarReport()
    {
        $.ajax({
            url   : 'http://localhost:8000/api/dashboard/daily',
            type  : 'GET',
            async : true,
            dataType : 'json',
            success : function(datas){  
                const categories = datas.data.map((o) => String(o.tanggal))
                const data_total = datas.data.map((o) => Number(o.total))
                const data_closed = datas.data.map((o) => Number(o.closed))
                const data_cancel = datas.data.map((o) => Number(o.cancel))
                Highcharts.chart('graph', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Antrian'
                    },
                    subtitle: {
                        text: 'total antrian'
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                        text: ''
                        }
                    },
                    plotOptions: {
                        line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        {
                            name: "total",
                            data: data_total
                        },
                        {
                            name: "closed",
                            data: data_closed
                        },
                        {
                            name: "cancel",
                            data: data_cancel
                        },
                    ]
                });
            } 

        });
    } //getBarReport

</script>


@endsection

