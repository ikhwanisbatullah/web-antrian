@extends('Admin/masterAdmin')

@section('judul_tab', 'Dashboard-Admin')
    
@section('active_menu_dashboard', 'active')

@section('css_after')
<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Daftar Bandara</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Data Bandara</a></li>
                            <li class="active">Bandara</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                        
                            <div class="card-body">
<!-- daepicker-->
<!-- <div class="form-group">
   <label>Tgl Awal</label>
   <div class="input-group date">
    <div class="input-group-addon">
           <span class="glyphicon glyphicon-th"></span>
       </div>
       <input id="tgl_mulai" placeholder="masukkan tanggal Awal" type="text" class="form-control datepicker" name="tgl_awal">
   </div>
  </div>
  <div class="form-group">
   <label>Tgl Akhir</label>
   <div class="input-group date">
    <div class="input-group-addon">
           <span class="glyphicon glyphicon-th"></span>
       </div>
       <input id="tgl_akhir" placeholder="masukkan tanggal Akhir" type="text" class="form-control datepicker" name="tgl_akhir">
   </div>
</div> -->
<!-- /daepicker-->
<div class="container">
    <div class="row">
        <div class="col-md-2">
        <input type="date" id="date_from" name="date_from" class="form-control input-md">
        </div>
        <div class="col-md-2">
            <input type="date" class="form-control input-md" id="date_to" name="date_to">
        </div>
        
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-sm" onClick="call_graph(); return false;" id="SUBMIT" name="search" >Search</button>
            {{-- <button type="submit" class="btn btn-success btn-sm" name="exportExcel" >Export excel</button> --}}
        </div>
    </div>

                                    {{-- <input type="date" id="date_from" name="date_from" class="form-control"  style="width: 220px;">
                                    <input type="date" id="date_to" name="date_to" class="form-control" style="width: 220px;">
                                    <button class="btn btn-primary" onClick="call_graph(); return false;" id="SUBMIT"
                                        style="margin-left: 20px;">SUBMIT</button>     --}}


                                    <div class="col-lg-11">
                                        <div id="test"> </div>
                                    </div>
                                <div class="col-lg-3 col-md-6">
                                    <!-- <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahKategoriBandara"><i class="fa fa-plus-square"></i>
                                    Tambah Bandara
                                    </button> -->
                                </div>
  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- <div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            
            <input type="date" name="date" class="form-control" style="width: 220px;">


            <button class="btn btn-primary" onClick="call_graph(); return false;" id="SUBMIT"
                style="margin-left: 20px;">SUBMIT</button>    
            
            
            <div class="col-lg-11">
                <div id="test"> </div>
            </div>
        </div>
    </div>
</div> -->
@endsection



@section('js_after')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script type="text/javascript">
    // datepicker
    // $(function(){
    //     $(".datepicker").datepicker({
    //         format: 'yyyy-mm-dd',
    //         autoclose: true,
    //         todayHighlight: true,
    //     });
    //     $("#tgl_mulai").on('changeDate', function(selected) {
    //         var startDate = new Date(selected.date.valueOf());
    //         $("#tgl_akhir").datepicker('setStartDate', startDate);
    //         if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
    //         $("#tgl_akhir").val($("#tgl_mulai").val());
    //         }
    //     });
    // });
    // datepicker

    var date = "{{date('Y-m-d')}}";
    getBarReport(date);

    function call_graph() 
    {
        date_from = $("[name='date_from']").val();
        date_to = $("[name='date_to']").val();
        getBarReport(date_from, date_to);

    }

    function getBarReport(date_from, date_to)
    {
        // alert('http://localhost:8000/api/grafik?date_from='+date_from+'&date_to='+date_to);
        $.ajax({
            url   : 'http://localhost:8000/api/grafik?date_from='+date_from+'&date_to='+date_to,
            // url   : 'https://next.json-generator.com/api/json/get/EkqMKs2J9',
            type  : 'GET',
            async : true,
            dataType : 'json',
            success : function(datas){  
                console.log(datas)
                const categories = datas.data.map((o) => String(o.tanggal))
                const data_closed = datas.data.map((o) => Number(o.closed))
                const data_cancel = datas.data.map((o) => Number(o.cancel))
                Highcharts.chart('test', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Antrian'
                    },
                    subtitle: {
                        text: 'total antrian'
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                        text: ''
                        }
                    },
                    plotOptions: {
                        line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        {
                            name: "closed",
                            data: data_closed
                        },
                        {
                            name: "cancel",
                            data: data_cancel
                        },
                    ]
                });
            } 

        });
    } //getBarReport

</script>


@endsection

