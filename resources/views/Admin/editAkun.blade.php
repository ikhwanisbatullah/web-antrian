@extends('Admin/masterAdmin')

@section('judul_tab', 'Konten - AdminBLK')
    
@section('active_menu_kelola_konten', 'active')

@section('content')

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Akun</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Data Akun</a></li>
                                    <li class="active">Akun</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <form action="/admin/dataAkun/updateAkun/{{$member->id}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="card-header">
                                    <strong class="card-title">Ubah Data Akun</strong>
                                </div>
                        
                                <div class="card-body">
                                    {{csrf_field()}}
                                    
                                    <div class="row form-group">
                                        <div class="col col-12">
                                            <label><strong>INFORMASI AKUN</strong></label>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Username</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="username" name="username" placeholder="Username" value="{{$member->username}}"class="form-control" readOnly>
                                            <small class="form-text text-muted">Tuliskan username!</small>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Password</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="password" id="password" name="password" placeholder="Password" value="{{$member->password}}" class="form-control" required readOnly>
                                            <small class="form-text text-muted"></small>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Email</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="email" name="email" placeholder="Masukkan Email" value="{{$member->email}}" class="form-control" required>
                                            <small class="form-text text-muted">Tuliskan email!</small>
                                        </div>
                                    </div>  

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Bandara</label></div>
                                        <div class="col-12 col-md-9">
                                            <select name="bandara_id" id="bandara_id" class="form-control" required>
                                                <option value="">---Pilih Bandara---</option>
                                                @foreach($kategori_bandara as $bandara)
                                                <option value="{{$bandara->id}}" @if($bandara->id == $member->bandara_id) selected @endif>{{$bandara->name}}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type='text' value="ground staff"> --}}
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Level Akun</label></div>
                                        <div class="col-12 col-md-9">
                                            <select name="level_user" id="level_user" class="form-control">
                                                <option value="">---Pilih Level Akun---</option>
                                                <option value="super-admin" @if($member->level_user =='super-admin') selected @endif>Super-admin</option>
                                                <option value="administrator" @if($member->level_user =='administrator') selected @endif>Administrator</option>
                                            </select>
                                            {{-- <input type='text' value="ground staff"> --}}
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" value="{{$profile->nama_lengkap}}" class="form-control" required>
                                            <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Nomor Kontak</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan nomor kontak" value="{{$profile->no_hp}}" class="form-control">
                                            <small class="form-text text-muted">Tuliskan nomor kontak!</small>
                                        </div>
                                    </div>
                                    
                                    

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Tanggal Lahir</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <input type="date" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir"value="{{$profile->tgl_lahir}}"  class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Alamat Lengkap</label>
                                        </div>
                                        <div class="col-12 col-md-9">
                                            <textarea id="alamat_rumah" name="alamat_rumah" placeholder="Masukkan Alamat Lengkap" class="form-control" required>{{($profile->alamat_rumah)}}</textarea>
                                            <small class="form-text text-muted">Tuliskan alamat lengkap!</small>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Jenis Kelamin</label></div>
                                        <div class="col-12 col-md-9">
                                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                                <option value="">---Pilih Jenis Kelamin---</option>
                                                <option value="laki-laki"  @if($profile->jenis_kelamin =='laki-laki') selected @endif>Laki-Laki</option>
                                                <option value="perempuan"  @if($profile->jenis_kelamin =='perempuan') selected @endif>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>            


                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Ubah</button> 
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/standard/ckeditor.js"></script>  --}}
    <script src="https://cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>
    
    {{-- <script>CKEDITOR.replace( 'isi_konten' ); </script> --}}
    {{-- <script src="https://cdn.tiny.cloud/1/cn0rsfqf5862dtcrgnngsfyi4vmj1ketcg7q1gtaw5w115xh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}
     <script>tinymce.init({selector:'textarea', height: 300});</script> 

    {{-- <script>
        ClassicEditor
                .create( document.querySelector( '#isi_konten' ) )
                .then( editor => {
                        console.log( editor );
                } )
                .catch( error => {
                        console.error( error );
                } );
    </script> --}}

    <script>

        $('#tgl_rilis').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_rilis2').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>
    

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahKonten').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var kd_konten = button.data('kd_konten');
              var judul_konten = button.data('judul_konten');
              var isi_konten = button.data('isi_konten');
              var kd_kategori_konten = button.data('kd_kategori_konten');
              var tgl_rilis = button.data('tgl_rilis');
             
              tinymce.activeEditor.setContent(isi_konten)

              var modal = $(this);
              modal.find('.modal-body #kd_konten').val(kd_konten);
              modal.find('.modal-body #judul_konten').val(judul_konten);
              modal.find('.modal-body #kd_kategori_konten').val(kd_kategori_konten);
              modal.find('.modal-body #isi_konten').val(isi_konten);
              modal.find('.modal-body #tgl_rilis2').val(tgl_rilis);
            });
        }); 
    </script> 

@endsection