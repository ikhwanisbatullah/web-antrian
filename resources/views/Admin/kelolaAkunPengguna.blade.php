@extends('Admin/masterAdmin')

@section('judul_tab', 'Akun Pengguna - Admin')
    
@section('active_menu_kelola_akun', 'active')

@section('content')
@if(auth()->user()->level_user =='super-admin')

        {{-- <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Akun Pengguna</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Data Pengguna</a></li>
                                    <li class="active">Akun Pengguna</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        {{-- <div class="right">
            <a type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#importSiswa">
            Import XLS
            </a>
                <a href="/admin/export" class="btn btn-sm btn-primary">Export Excel</a>
                <a href="/siswa/exportpdf" class="btn btn-sm btn-primary">Export Pdf</a>
                <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"><i class="lnr lnr-plus-circle"></i>
                </button>
            </div> --}}

        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Kelola Akun Pengguna</strong>
                            </div>
                        
                            <div class="card-body">

                                <div class="col-lg-3 col-md-6">
                                    <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahAkun"><i class="fa fa-plus-square"></i>
                                    Tambah Akun
                                    </button>
                                </div>
                                {{-- <div class="col-lg-3 col-md-6"> 
                                    <button type="submit" class="btn btn-info mb-2 ata-toggle=" href="/admin/export" ><i></i>
                                    Export to Excel
                                    </button>
                                </div> --}}
                                <div class="col-md-12">
                                    <!-- BASIC TABLE -->
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <div class="right">
                                                <a href="/admin/export" class="btn btn-sm btn-primary">Export Excel</a>
                                               
                                            </div>
                                        </div>	
                                    </div>
                                    <!-- END BASIC TABLE -->
                                

                                <!-- Modal Tambah Akun-->

                                <div class="modal fade" id="tambahAkun" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Tambah Akun</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataAkun/tambahAkun') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    {{ csrf_field()}}

                                                    

                                                    <div class="row form-group">
                                                        <div class="col col-12">
                                                            <label><strong>INFORMASI AKUN</strong></label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Username</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="username" name="username" placeholder="Username" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan username!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Password</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="password" id="password" name="password" placeholder="Password" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan password!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Email</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="email" name="email" placeholder="Masukkan Email" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan email!</small>
                                                        </div>
                                                    </div>  

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Bandara</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="bandara_id" id="bandara_id" class="form-control" required>
                                                                <option value="">---Pilih Bandara---</option>
                                                                @foreach($kategori_bandara as $bandara)
                                                                <option value="{{$bandara->bandara_id}}">{{$bandara->bandara_name}}</option>
                                                                @endforeach
                                                            </select>
                                                            {{-- <input type='text' value="ground staff"> --}}
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Level Akun</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="level_user" id="level_user" class="form-control">
                                                                <option value="">---Pilih Level Akun---</option>
                                                                <option value="super-admin">Super-admin</option>
                                                                <option value="administrator">Administrator</option>
                                                            </select>
                                                            {{-- <input type='text' value="ground staff"> --}}
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nomor Kontak</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan nomor kontak" class="form-control">
                                                            <small class="form-text text-muted">Tuliskan nomor kontak!</small>
                                                        </div>
                                                    </div>
                                                    
                                                    {{-- <div class="row form-group">
                                                        <div class="col col-md-3"><label for="tempat_lahir" class=" form-control-label">Tempat Lahir</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select class="form-control js-example-basic-single" name="tempat_lahir" id="tempat_lahir" style="width:100%">
                                                                <option value="">---Pilih Tempat Lahir---</option>
                                                                @foreach($kota as $city)
                                                                <option value="{{$city->id}}">{{$city->type." ".$city->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div> --}}

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Tempat Lahir</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" class="form-control">
                                                            <small class="form-text text-muted">Tempat Lahir!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Tempat Lahir</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="date" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" class="form-control">
                                                            <small class="form-text text-muted">Tanggal Lahir!</small>
                                                        </div>
                                                    </div>

                                                    {{-- <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Tanggal Lahir</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" class="form-control" required readonly>
                                                        </div>
                                                    </div> --}}

                                                    {{-- <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Tanggal Lahir</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="date" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" class="form-control" required>
                                                        </div>
                                                    </div> --}}

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Alamat Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <textarea id="alamat_rumah" name="alamat_rumah" placeholder="Masukkan Alamat Lengkap" class="form-control" required> </textarea>
                                                            <small class="form-text text-muted">Tuliskan alamat lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Jenis Kelamin</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                                                <option value="">---Pilih Jenis Kelamin---</option>
                                                                <option value="laki-laki">Laki-Laki</option>
                                                                <option value="perempuan">Perempuan</option>
                                                            </select>
                                                        </div>
                                                    </div>            

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal Ubah Akun -->
                                {{-- <div class="modal fade" id="ubahAkun" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Ubah Akun</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataAkun/ubahAkun') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    {{ csrf_field()}}

                                                    <div class="row form-group" hidden>
                                                        <div class="col col-md-3">
                                                            <label for="number-input" class=" form-control-label">Kode Pengguna</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input readonly type="number" id="id" name="id" placeholder="Masukkan ID Kelas " class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-12">
                                                            <label><strong>INFORMASI MEMBER</strong></label>
                                                        </div>
                                                    </div>        


                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nomor Handphone</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan Nomor Handphone" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nomor handphone!</small>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div> --}}

                                <br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nomor Handphone</th>
                                            <th>Alamat Rumah</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($profile as $profile)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$profile->nama_lengkap}}</td>
                                            <td>{{$profile->no_hp}}</td>
                                            <td>{{$profile->alamat_rumah}}</td>
                                            <td>{{$profile->jenis_kelamin}} dasdsa</td>
                                            <td>
                                            <a href="{{url('admin/dataAkun/editAkun').'/'.$profile->user_id}}" class="btn btn-success btn-sm"> <i class="fa fa-edit"></i> Ubah</a>
                                            
                                            <a href="{{url('admin/dataAkun/hapusAkun').'/'.$profile->user_id}}" type="button" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>&nbsp;
                                                Hapus
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $('.js-example-basic-single').select2({
            theme: "classic",
        });
    </script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script>
        $('#tgl_lahir').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_lahir2').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>
        
    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi2').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota2').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota2').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    
    <script type="text/javascript">
        $(document).ready(function(){
            $('#kabupaten_kota2').on('change', function(){
                $.ajax({
                    url: "/ajax-kodepos",
                    type:"POST",
                    data : {"_token": "{{ csrf_token() }}",
                    "id":$(this).val()},
                    dataType: "json",
                    success: function(res){
                        console.log(res.kodepos);
                        $('#kodepos2').val(res.kodepos);
                    }
                })
              
            })

            // init
            $('#kabupaten_kota2').change();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahAkun').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var id = button.data('id');
              var nama_lengkap = button.data('nama_lengkap');
              var no_hp =  button.data('no_hp');
             
              var modal = $(this);
              modal.find('.modal-body #id').val(id);
              modal.find('.modal-body #nama_lengkap ').val(nama_lengkap);
              modal.find('.modal-body #no_hp').val(no_hp);
        }); 
    </script> 

@endif
@if(auth()->user()->level_user =='administrator')

        // <div class="breadcrumbs">
        //     <div class="breadcrumbs-inner">
        //         <div class="row m-0">
        //             <div class="col-sm-4">
        //                 <div class="page-header float-left">
        //                     <div class="page-title">
        //                         <h1>Akun Pengguna</h1>
        //                     </div>
        //                 </div>
        //             </div>
        //             <div class="col-sm-8">
        //                 <div class="page-header float-right">
        //                     <div class="page-title">
        //                         <ol class="breadcrumb text-right">
        //                             <li><a href="#">Dashboard</a></li>
        //                             <li><a href="#">Data Pengguna</a></li>
        //                             <li class="active">Akun Pengguna</li>
        //                         </ol>
        //                     </div>
        //                 </div>
        //             </div>
        //         </div>
        //     </div>
        // </div>

        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Kelola Akun Pengguna</strong>
                            </div>
                        
                            <div class="card-body">

                                <div class="col-lg-3 col-md-6">
                                    <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahAkun"><i class="fa fa-plus-square"></i>
                                    Tambah Akun
                                    </button>
                                </div>
                                

                                <!-- Modal Tambah Akun-->

                                <div class="modal fade" id="tambahAkun" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Tambah Akun</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataAkun/tambahAkun') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    {{ csrf_field()}}

                                                    

                                                    <div class="row form-group">
                                                        <div class="col col-12">
                                                            <label><strong>INFORMASI AKUN</strong></label>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Username</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="username" name="username" placeholder="Username" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan username!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Password</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="password" id="password" name="password" placeholder="Password" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan password!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Email</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="email" name="email" placeholder="Masukkan Email" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan email!</small>
                                                        </div>
                                                    </div>  

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Bandara</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="bandara_id" id="bandara_id" class="form-control" required>
                                                                <option value="">---Pilih Bandara---</option>
                                                                @foreach($kategori_bandara as $bandara)
                                                                <option value="{{$bandara->id}}">{{$bandara->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            {{-- <input type='text' value="ground staff"> --}}
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Level Akun</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="level_user" id="level_user" class="form-control">
                                                                <option value="">---Pilih Level Akun---</option>
                                                                <option value="super-admin">Super-admin</option>
                                                                <option value="administrator">Administrator</option>
                                                            </select>
                                                            {{-- <input type='text' value="ground staff"> --}}
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nomor Kontak</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan nomor kontak" class="form-control">
                                                            <small class="form-text text-muted">Tuliskan nomor kontak!</small>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="tempat_lahir" class=" form-control-label">Tempat Lahir</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select class="form-control js-example-basic-single" name="tempat_lahir" id="tempat_lahir" style="width:100%">
                                                                <option value="">---Pilih Tempat Lahir---</option>
                                                                @foreach($kota as $city)
                                                                <option value="{{$city->id}}">{{$city->type." ".$city->nama}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Tanggal Lahir</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" class="form-control" required readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Alamat Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <textarea id="alamat_rumah" name="alamat_rumah" placeholder="Masukkan Alamat Lengkap" class="form-control" required> </textarea>
                                                            <small class="form-text text-muted">Tuliskan alamat lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3"><label for="text_input" class=" form-control-label">Jenis Kelamin</label></div>
                                                        <div class="col-12 col-md-9">
                                                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                                                <option value="">---Pilih Jenis Kelamin---</option>
                                                                <option value="laki-laki">Laki-Laki</option>
                                                                <option value="perempuan">Perempuan</option>
                                                            </select>
                                                        </div>
                                                    </div>            

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal Ubah Akun -->
                                <div class="modal fade" id="ubahAkun" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Ubah Akun</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataAkun/ubahAkun') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    {{ csrf_field()}}

                                                    <div class="row form-group" hidden>
                                                        <div class="col col-md-3">
                                                            <label for="number-input" class=" form-control-label">Kode Pengguna</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input readonly type="number" id="id" name="id" placeholder="Masukkan ID Kelas " class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-12">
                                                            <label><strong>INFORMASI MEMBER</strong></label>
                                                        </div>
                                                    </div>        


                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nomor Handphone</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan Nomor Handphone" class="form-control" required>
                                                            <small class="form-text text-muted">Tuliskan nomor handphone!</small>
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                

                                <br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nomor Handphone</th>
                                            <th>Alamat Rumah</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($profile as $profile)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$profile->nama_lengkap}}</td>
                                            <td>{{$profile->no_hp}}</td>
                                            <td>{{$profile->alamat_rumah}}</td>
                                            <td>{{$profile->jenis_kelamin}}</td>
                                            <td>
                                            <button type="button" class="btn btn-success btn-sm" 
                                                data-target="#ubahAkun" 
                                                
                                                data-toggle="modal"
                                                data-kd_pengguna="{{$profile->kd_pengguna}}"
                                                data-nomor_ktp="{{$profile->nomor_ktp}}"
                                                data-nama_lengkap="{{$profile->nama_lengkap}}"
                                                data-jenis_kelamin="{{$profile->jenis_kelamin}}"
                                                data-tempat_lahir="{{$profile->tempat_lahir}}"
                                                data-tgl_lahir="{{$profile->tgl_lahir}}"
                                                data-pend_terakhir="{{$profile->pend_terakhir}}"
                                                data-thn_ijazah="{{$profile->thn_ijazah}}"
                                                data-alamat_lengkap="{{$profile->alamat_lengkap}}"
                                                data-provinsi="{{$profile->provinsi}}"
                                                data-kabupaten_kota="{{$profile->kabupaten_kota}}"
                                                data-kodepos="{{$profile->kodepos}}"
                                                data-nomor_kontak="{{$profile->nomor_kontak}}"
                                                data-ukuran_baju="{{$profile->ukuran_baju}}"
                                                data-ukuran_sepatu="{{$profile->ukuran_sepatu}}"
                                                data-username="{{$profile->username}}"
                                                data-password="{{$profile->password}}"
                                                data-email="{{$profile->email}}" 
                                            >
                                                <i class="fa fa-edit"></i>&nbsp; 
                                                Ubah
                                            </button>
                                            <a href="/admin/dataAkun/hapusAkun/{{$profile->user_id}}" type="button" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>&nbsp;
                                                Hapus
                                            </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script>
        $('.js-example-basic-single').select2({
            theme: "classic",
        });
    </script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

    <script>
        $('#tgl_lahir').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_lahir2').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>
        
    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi2').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota2').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota2').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    
    <script type="text/javascript">
        $(document).ready(function(){
            $('#kabupaten_kota2').on('change', function(){
                $.ajax({
                    url: "/ajax-kodepos",
                    type:"POST",
                    data : {"_token": "{{ csrf_token() }}",
                    "id":$(this).val()},
                    dataType: "json",
                    success: function(res){
                        console.log(res.kodepos);
                        $('#kodepos2').val(res.kodepos);
                    }
                })
              
            })

            // init
            $('#kabupaten_kota2').change();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahAkun').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var kd_pengguna = button.data('kd_pengguna');
              var nama_lengkap = button.data('nama_lengkap');
              var no_hp =  button.data('no_hp');
              var tempat_lahir =  button.data('tempat_lahir');
              var pend_terakhir = button.data('pend_terakhir');
              var thn_ijazah = button.data('thn_ijazah');
              var tgl_lahir = button.data('tgl_lahir');
              var alamat_lengkap = button.data('alamat_lengkap');
              var provinsi = button.data('provinsi');
              var kabupaten_kota = button.data('kabupaten_kota');
              var kodepos = button.data('kodepos');
              var nomor_kontak = button.data('nomor_kontak');
              var ukuran_baju = button.data('ukuran_baju');
              var ukuran_sepatu = button.data('ukuran_sepatu');
              var username = button.data('username');
              var password = button.data('password');
              var email = button.data('email');
             
              var modal = $(this);
              modal.find('.modal-body #kd_pengguna').val(kd_pengguna);
              modal.find('.modal-body #nama_lengkap').val(nama_lengkap);
              modal.find('.modal-body #no_hp').val(no_hp);
              modal.find('.modal-body #tgl_lahir2').val(tgl_lahir);
              modal.find('.modal-body #jenis_kelamin').val(jenis_kelamin);
              modal.find('.modal-body #alamat_lengkap').val(alamat_lengkap);
              modal.find('.modal-body #pend_terakhir').val(pend_terakhir);
              modal.find('.modal-body #thn_ijazah').val(thn_ijazah);
              modal.find('.modal-body #provinsi2').val(provinsi);
              modal.find('.modal-body #kabupaten_kota2').val(kabupaten_kota);
              modal.find('.modal-body #kodepos2').val(kodepos);
              modal.find('.modal-body #nomor_kontak').val(nomor_kontak);
              modal.find('.modal-body #ukuran_baju_lain').val(ukuran_baju);
              modal.find('.modal-body #ukuran_sepatu_lain').val(ukuran_sepatu);
              modal.find('.modal-body #username').val(username);
              modal.find('.modal-body #password').val(password);
              modal.find('.modal-body #email').val(email);
            });
        }); 
    </script> 

@endif

@endsection

