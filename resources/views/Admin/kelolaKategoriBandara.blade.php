@extends('Admin/masterAdmin')

@section('judul_tab', 'Kategori Bandara - Admin')
    
@section('active_menu_kelola_kategori_bandara', 'active')

@section('content')

        {{-- <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Daftar Bandara</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Data Bandara</a></li>
                                    <li class="active">Bandara</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Kelola Data Bandara</strong>
                            </div>
                        
                            <div class="card-body">

                                <div class="col-lg-3 col-md-6">
                                    <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahKategoriBandara"><i class="fa fa-plus-square"></i>
                                    Tambah Bandara
                                    </button>
                                </div>

                                <!-- Modal Tambah Kategori Bandara -->

                                <div class="modal fade" id="tambahKategoriBandara" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Tambah Bandara</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataBandara/kategoriBandara/tambahKategoriBandara')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    {{ csrf_field()}}

                                                    <div class="row form-group">
                                                        <div class="col col-md-3" hidden>
                                                            <label for="number-input" class=" form-control-label">Kode Bandara</label>
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Bandara</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="bandara_name" name="bandara_name" placeholder="Masukkan Nama Bandara" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Keterangan</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="bandara_desc" name="bandara_desc" placeholder="Masukkan Keterangan Tambahan" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Tambah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal Ubah KategoriBandara -->
                                <div class="modal fade" id="ubahKategoriBandara" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Ubah Data Bandara</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="/admin/dataBandara/kategoriBandara/ubahKategoriBandara" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    
                                                    {{ csrf_field()}}

                                                    <div class="row form-group" hidden>
                                                        <div class="col col-md-3">
                                                            <label for="number-input" class=" form-control-label">Kode Kategori</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="number" id="bandara_id" name="bandara_id" class="form-control" readonly>
                                                        </div>
                                                    </div>

                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Nama Bandara</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="bandara_name" name="bandara_name" placeholder="Masukkan Nama Bandara" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col col-md-3">
                                                            <label for="text-input" class=" form-control-label">Keterangan</label>
                                                        </div>
                                                        <div class="col-12 col-md-9">
                                                            <input type="text" id="bandara_desc" name="bandara_desc" placeholder="Masukkan Ketarangan Tambahan" class="form-control" required>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Bandara</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($kategori_bandara as $bandara)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$bandara->bandara_name}}</td>
                                            <td>
                                                <button type="button" class="btn btn-success btn-sm" 
                                                    data-target="#ubahKategoriBandara" 
                                                    data-toggle="modal"
                                                    data-bandara_id="{{$bandara->bandara_id}}"
                                                    data-bandara_name="{{$bandara->bandara_name}}"
                                                    data-bandara_desc="{{$bandara->bandara_desc}}"
                                                >
                                                    <i class="fa fa-edit"></i>&nbsp; 
                                                    Ubah
                                                </button>
                                                <a href="/admin/dataBandara/kategoriBandara/hapusKategoriBandara/{{$bandara->bandara_id}}" type="button" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>&nbsp;
                                                    Hapus
                                                </a>
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahKategoriBandara').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var bandara_id = button.data('bandara_id');
              var bandara_name = button.data('bandara_name');
              var bandara_desc = button.data('bandara_desc');
             
              var modal = $(this);
              modal.find('.modal-body #bandara_id').val(bandara_id);
              modal.find('.modal-body #bandara_name').val(bandara_name);
              modal.find('.modal-body #bandara_desc').val(bandara_desc);
            });
        }); 
    </script> 

@endsection