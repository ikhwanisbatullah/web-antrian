@extends('Admin/masterAdmin')

@section('judul_tab', 'Lokasi - Admin')
    
@section('active_menu_kelola_order', 'active')

@section('content')

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Order</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li><a href="#">Data Order</a></li>
                                    <li class="active">Order</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">

                    {{-- <div class="col-md-12">
                        <div class="card">
                            <form action="{{ url('/admin/dataLokasi/tambahLokasi')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="card-header">
                                    <strong class="card-title">Tambah Lokasi</strong>
                                </div>
                        
                                <div class="card-body">
                                    {{csrf_field()}}

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="id" class=" form-control-label">Bandara</label>
                                            <select name="id" id="id" class="form-control" required>
                                                <option value="">---Pilih Bandara---</option>
                                                @foreach($kategori_bandara as $bandara)
                                                <option value="{{$bandara->id}}">{{$bandara->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="name" class=" form-control-label">Nama Gerbang</label>
                                            <input type="text" id="name" name="name" placeholder="Masukkan Nama Gerbang" class="form-control">
                                        </div>
                                    </div>
                        
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Tambahkan</button> 
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div> --}}

                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <strong class="card-title">Data Order</strong>
                            </div>

                            <div class="card-body">

                                <!-- Modal Ubah Lokasi -->
                                <div class="modal fade" id="ubahLokasi" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h3 class="modal-title" id="mediumModalLabel"><strong>Ubah Lokasi</strong></h3>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>      
                                            <div class="modal-body">
                                                <form action="{{ url('/admin/dataLokasi/ubahLokasi') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                    
                                                    {{ csrf_field()}}

                                                    <div class="col-lg-12" hidden>
                                                        <div class="form-group">
                                                            <label for="id" class=" form-control-label">Kode Bandara</label>
                                                            <input type="text" id="id" name="kd_konten" class="form-control" readonly required>
                                                        </div>
                                                    </div>
                   
                                                    {{-- <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="id" class=" form-control-label">Bandara</label>
                                                            <select name="id" id="id" class="form-control" required>
                                                                <option value="">---Pilih Bandara---</option>
                                                                @foreach($kategori_bandara as $bandara)
                                                                <option value="{{$bandara->id}}">{{$bandara->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="name" class=" form-control-label">Nama Gerbang</label>
                                                            <input type="text" id="name" name="name" placeholder="Masukkan Nama Gerbang" class="form-control" required>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                                    </div>
                                                </form>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Bandara</th>
                                            <th>Nama Gerbang</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lokasi as $lokasi)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{App\Models\Bandara::where('id', $lokasi->id_bandara)->value('name')}}</td>
                                            <td>{{$lokasi->name}}</td>
                                            <td>
                                                <button type="button" class="btn btn-success btn-sm" 
                                                    data-target="#ubahLokasi" 
                                                    data-toggle="modal"
                                                    data-id="{{$lokasi->id}}"
                                                    data-name="{{$lokasi->name}}"
                                                    data-id="{{$lokasi->id_bandara}}">
                                                    <i class="fa fa-edit"></i>&nbsp; 
                                                    Ubah
                                                </button>
                                                <a href="/admin/dataLokasi/hapusLokasi/{{$lokasi->id}}" type="button" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>&nbsp;
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> 
    <script src="https://cdn.tiny.cloud/1/cn0rsfqf5862dtcrgnngsfyi4vmj1ketcg7q1gtaw5w115xh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea', height: 300});</script>

    <script>
        $('#tgl_rilis').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_rilis2').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahLokasi').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var id = button.data('id');
              var id_bandara = button.data('id_bandara');
              var name = button.data('name');

              var modal = $(this);
              modal.find('.modal-body #id').val(id);
              modal.find('.modal-body #id_bandara').val(id_bandara);
              modal.find('.modal-body #name').val(name);
            });
        }); 
    </script> 

@endsection