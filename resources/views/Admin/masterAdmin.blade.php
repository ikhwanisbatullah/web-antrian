<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('judul_tab')</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon" />
    
    <link rel="stylesheet" href="{{url('assets/css/normalize.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/bootstrap.min.css')}}">
    <link href="{{url('fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{url('fontawesome/css/brands.css')}}" rel="stylesheet">
    <link href="{{url('fontawesome/css/solid.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/pe-icon-7-stroke.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/lib/datatable/dataTables.bootstrap.min.css')}}">
    <link href="{{url('assets/css/jqvmap.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/fullcalendar.min.css')}}" rel="stylesheet">
    <link href="{{url('assets/css/gijgo.min.css')}}" rel="stylesheet">
    <!-- link public -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- link public -->
    <link href="{{url('assets/css/select2.min.css')}}" rel="stylesheet">
    
    <link rel="stylesheet" href="{{url('assets/css/cs-skin-elastic.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link rel="stylesheet" href="{{url('assets/css/chartist.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/css/style.css')}}">

    @yield('css_after')

<body> 
<!-- /#left-panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="menu-title">Data Aplikasi Antrian</li><!-- /.menu-title -->

                    @if(auth()->user()->level_user =='super-admin'  or auth()->user()->level_user =='admin-dashboard')
                    <li class="@yield('active_menu_dashboard')">
                        <a href="{{url('dashboard')}}"><i class="menu-icon fas fa-chart-pie"></i>Dashboard </a>
                    </li>
                    @endif

                    @if(auth()->user()->level_user =='super-admin' or auth()->user()->level_user =='administrator')
                    <!-- <li class="@yield('active_menu_report')">
                        <a href="{{url('admin/report')}}"><i class="menu-icon fa fa-wpforms"></i>Report</a>
                    </li> -->
                    <!-- <li class="@yield('active_menu_chart')">
                        <a href="{{url('admin/chart')}}"><i class="menu-icon fa fa-area-chart"></i>Chart</a>
                    </li> -->
                    <li class="@yield('active_menu_kelola_akun')">
                        <a href="{{url('admin/dataAkun/akun')}}"><i class="menu-icon fa fa-male"></i>Petugas</a>
                    </li>
                    @endif
                    
                    

                    @if(auth()->user()->level_user =='super-admin')
                    <li class="@yield('active_menu_kelola_kategori_bandara')">
                        <a href="{{url('admin/dataBandara/kategoriBandara')}}"><i class="menu-icon fas fa-plane"></i>Bandara</a>
                    </li>
                    @endif

                    @if(auth()->user()->level_user =='super-admin' or auth()->user()->level_user =='administrator')
                    <li class="@yield('active_menu_kelola_lokasi')">
                        <a href="{{url('admin/dataLokasi/Lokasi')}}"><i class="menu-icon fa fa-bars"></i>Terminal</a>
                    </li>
                    @endif
                    
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>

    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">

            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand width: 5px; height: 500px" href="#"><img src="{{url('images/ic_logo_main.png')}}" style="max-width: 100px;" alt="Logo"></a>
                    <a class="navbar-brand hidden" href="./"><img src="{{url('images/logo2.png')}}" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="user-area dropdown float-right">
                        <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="{{url('images/boss.png')}}" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                            <a class="nav-link" href="{{url('admin/gantiPassword')}}"><i class="fa fa-power -off"></i>Ganti Password</a>
                            <a class="nav-link" href="{{url('logout')}}"><i class="fa fa-power -off"></i>Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- /#header -->

        <!-- Content -->
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session()->has('alert success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('alert success')}}
            </div>
        @endif

        @if (session()->has('alert danger'))
            <div class="alert alert-danger" role="alert">
                {{session()->get('alert danger')}}
            </div>
        @endif

        @if (session()->has('alert warning'))
            <div class="alert alert-warning" role="alert">
                {{session()->get('alert warning')}}
            </div>
        @endif 
        
        @if (Session()->has('alert modal success'))
            <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body alert-success">
                    <p><h5>{{session()->get('alert modal success')}}</h5></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
          @endif

          @if (Session()->has('alert modal danger'))
            <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body alert-danger">
                    <p><h5>{{session()->get('alert modal danger')}}</h5></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
          @endif

          @if (Session()->has('alert modal warning'))
            <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-body alert-warning">
                    <p><h5>{{session()->get('alert modal warning')}}</h5></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
          @endif

        @yield('content')
        <!-- /.content -->

        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019 PT. Pilar Info Nusantara
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->
    </div>

    <!-- /#right-panel -->
    <!-- Scripts -->
    
    <script src="{{url('assets/js/lib/data-table/datatables.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/jszip.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/vfs_fonts.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/buttons.print.min.js')}}"></script>
    <script src="{{url('assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
    <script src="{{url('assets/js/init/datatables-init.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
   
    <script src="{{url('assets/js/popper.min.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
        $('#myModal').modal('show')
    </script>
     <script src="{{url('assets/js/popper.min.js')}}"></script>
     <script src="{{url('assets/js/moment.min.js')}}"></script>
     <script src="{{url('assets/js/fullcalendar.min.js')}}"></script>
     <script src="{{url('assets/js/jquery.matchHeight.min.js')}}"></script>
     <script src="{{url('assets/js/Chart.bundle.min.js')}}"></script>
     <script src="{{url('assets/js/jquery.flot.min.js')}}"></script>
     <script src="{{url('assets/js/jquery.flot.spline.min.js')}}"></script>
       
    {{-- <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script> --}}
    <script src="{{url('assets/js/init/fullcalendar-init.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="{{url('assets/js/main.js')}}"></script>

    {{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script> --}}
    <!--Flot Chart-->
    {{-- <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>  --}}
    <script src="{{url('assets/js/widgets.js')}}"></script>

    @yield('js_after')

    @yield('highcharts')

    

</body>
</html>
