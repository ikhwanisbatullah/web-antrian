@extends('Admin/masterAdmin')

@section('judul_tab', 'Kategori Bandara - Admin')
    
@section('active_menu_report', 'active')

@section('content')

       
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Kelola Data Report</strong>
                            </div>
                        
                            
                            <div class="card-body">
                                <form action="/admin/report" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    
                                    <div class="container">
    
                                    <div class="row">
                                    <label for="from" class="col-form-label">From</label>
                                        <div class="col-md-2">
                                        <input type="date" class="form-control input-sm" id="from" name="from" required>
                                        </div>
                                        <label for="to" class="col-form-label">To</label>
                                        <div class="col-md-2">
                                            <input type="date" class="form-control input-sm" id="to" name="to" required>
                                        </div>
                                        
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary btn-sm" name="search" >Search</button>
                                            <button type="submit" class="btn btn-success btn-sm" name="exportExcel" >Export excel</button>
                                        </div>
                                    </div>

                                </div>
                                </form>
                                <br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Bandara</th>
                                            <th>Nama Terminal</th>
                                            <th>Tanggal dan Waktu Order</th>
                                            <th style="width: 500px">Waktu Kendaraan Dikirim Ke Pick up Point</th>
                                            <th>Waktu Antrian Di Closed</th>
                                            <th>Waktu Tunggu Customer</th>
                                            <th>Nomor Antrian</th>
                                            <th>Kode OTP</th>
                                            <th>Pilihan Rute</th>
                                            <th>Status Pesanan</th>
                                            <th>Nomor Kendaraan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ViewsPage as $antrian)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$antrian->bandara}}</td>
                                            <td>{{$antrian->terminal}}</td>
                                            {{-- <td>{{$antrian->date_order}}</td>
                                            <td>{{$antrian->time_order}}</td> --}}
                                            <td>{{$antrian->tanggal_dan_waktu_order}}</td>
                                            <td>{{$antrian->kendaraan_kirim_ke_pickup_point}}</td>
                                            <td>{{$antrian->closed_antrian}}</td>
                                            <td>{{$antrian->waktu_tunggu_customer}}</td>
                                            <td>{{$antrian->nomor_antrian}}</td>
                                            <td>{{$antrian->kode_otp}}</td>
                                            <td>{{$antrian->pilihan_rute}}</td>
                                            <td>{{$antrian->status_pesanan}}</td>
                                            <td>{{$antrian->nomor_kendaraan}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahKategoriBandara').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var id = button.data('id');
              var name = button.data('name');
              var desc = button.data('desc');
             
              var modal = $(this);
              modal.find('.modal-body #id').val(id);
              modal.find('.modal-body #name').val(name);
              modal.find('.modal-body #desc').val(desc);
            });
        }); 
    </script> 

@endsection