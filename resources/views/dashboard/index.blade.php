@extends('Admin/masterAdmin')

@section('judul_tab', 'Lokasi - Admin')
    
@section('active_menu_dashboard', 'active')

@section('css_after')
<!-- datatable -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
<!-- datatable  -->

<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
<style>
.height-30px {
  /* width: 140px; */
  height: 35px; 
}
</style>
@endsection

@section('content')

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <!-- <li class="active">Dashboard</li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">


                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- <strong class="card-title">Dashboard</strong> -->
                            </div>

                            <div class="card-body">
                            <!-- content -->   
                            <div class="container">
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Dari tanggal:</label>
                                    <!-- <input type="date" id="date_from" name="date_from" class="form-control input-md"> -->
                                    <input type="text" id="date_from" name="date_from" aria-hidden="true" class="fa fa-calendar datepicker form-control height-30px">
                                </div>
                                <div class="col-md-2">
                                    <label>Sampai tanggal:</label>
                                    <!-- <input type="date" class="form-control input-md" id="date_to" name="date_to"> -->
                                    <input type="text" id="date_to" name="date_to" class="datepicker form-control height-30px">
                                </div>
                                <div class="col-md-2">
                                    <label>Terminal:</label>
                                    <input type="text" id="terminal" name="terminal" class="form-control height-30px">
                                </div>
                                <div class="col-md-2">
                                    <label>Granularity:</label>
                                    <select class="form-control height-30px" name="granularity">
                                        <option value="daily">daily</option>
                                        <option value="weekly">weekly</option>
                                        <option value="monthly">monthly</option>
                                    </select>
                                </div>
                                
                                <div class="col-md-3">
                                    <button class="btn btn-primary" onClick="call_graph(); return false;" id="SUBMIT" style="margin-top: 30px;">Search</button> 
                                    <button class="btn btn-success" onClick="callExcell(); return false;" style="margin-top: 30px;" name="exportExcel" >Export</button>
                                </div>
                            </div>
                            
                            <div style="padding-top: 50px;">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Bandara</th>
                                        <th>Terminal</th>
                                        <th>Total Orders</th>
                                        <th>Waktu tunggu</th>
                                    </tr>
                                </thead>
                            </table>
                            </div>
                            
                            <div class="col-lg-11" style="padding-top: 50px;">
                                <div id="test"> </div>
                            </div>
                            <div class="col-lg-11" style="padding-top: 50px;">
                                <div id="waiting"> </div>
                            </div>
                            <div class="col-lg-3 col-md-6">           
                            </div>
                    </div>
                            <!-- content -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('js_after')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<!-- datatables -->
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
<!-- datatables -->

<script type="text/javascript">
    // datepicker
    $(function(){
        $("#date_from").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
        $("#date_to").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
        // $(".datepicker").datepicker({
        //     format: 'yyyy-mm-dd',
        //     autoclose: true,
        //     todayHighlight: true,
        // });
        // $("#tgl_mulai").on('changeDate', function(selected) {
        //     var startDate = new Date(selected.date.valueOf());
        //     $("#date_to").datepicker('setStartDate', startDate);
        //     if($("#date_from").val() > $("#date_to").val()){
        //     $("#date_to").val($("#date_from").val());
        //     }
        // });
    });
    // datepicker

    $("[name='granularity']").on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        // alert(valueSelected);
        if(valueSelected = 'daily'){
            $("#ds-tgl").show();
            // alert('daily boss')
        }
        else if(valueSelected = 'weekly'){
            $("#ds-tgl").hide();
            // alert('weekly boss')

        }
        else if(valueSelected = 'monthly'){
            $("#ds-tgl").hide();
            // alert('monthly bos')
            
        }
    });

    var date = "{{date('Y-m-d')}}";
    getBarReport(date);
    show_datatables(date);

    function call_graph() 
    {
        date_from = $("[name='date_from']").val();
        date_to = $("[name='date_to']").val();
        terminal = $("[name='terminal']").val();
        granularity = $("[name='granularity']").val();
        show_datatables(date_from, date_to, terminal, granularity);
        getBarReport(date_from, date_to, terminal, granularity);
        // getBarWaiting(date_from, date_to, terminal, granularity);

    }

    function show_datatables(date_from, date_to, terminal, granularity)
    {
        $('#example').DataTable().destroy();
        var url_site = "{{url('/')}}"+"/api/dashboard/list?date_from="+date_from+'&date_to='+date_to+'&terminal='+terminal+'&granularity='+granularity;  
        $('#example').DataTable(
        {
            "retrieve": true,
            "destroy": true,
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": true,
            // "serverSide": true,
            // "order": [
            //     [0, 'desc']
            // ],
            "ajax": {
                url : url_site,
                dataType: "json",
                type : "GET"
            },
        });
    }

    function callExcell()
    {
        date_from = $("[name='date_from']").val();
        date_to = $("[name='date_to']").val();
        terminal = $("[name='terminal']").val();
        granularity = $("[name='granularity']").val();
        getExcell(date_from, date_to, terminal, granularity);
    }

    function getExcell(date_from, date_to, terminal, granularity)
    {
        var url = 'admin/chart/export?from='+date_from+'&to='+date_to+'&gran='+granularity+'&terminal='+terminal;
        window.open(url, '_blank');
    }

    function getBarReport(date_from, date_to, terminal, granularity)
    {
        var url_site = "{{url('')}}"+'/api/grafik?date_from='+date_from+'&date_to='+date_to+'&terminal='+terminal+'&granularity='+granularity;   
        $.ajax({
            url   : url_site,
            // url   : 'https://next.json-generator.com/api/json/get/EkqMKs2J9',
            type  : 'GET',
            async : true,
            dataType : 'json',
            success : function(datas){  
                // console.log(datas)
                const categories = datas.data.map((o) => String(o.tanggal))
                const data_total = datas.data.map((o) => Number(o.total))
                const data_closed = datas.data.map((o) => Number(o.closed))
                const data_cancel = datas.data.map((o) => Number(o.cancel))
                const data_waiting = datas.data.map((o) => Number(o.waiting))
                const data_otw = datas.data.map((o) => Number(o.otw))
                Highcharts.chart('test', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Antrian'
                    },
                    subtitle: {
                        text: 'total antrian'
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br />',
                        // pointFormatter: function() {
                        //         var point = this
                        //     if (point.state === 'hover') {
                        //             let cornerY = document.querySelector('.cornerY'),
                        //             value = point.y
                                
                        //         cornerY.innerHTML = "Y value: " + value
                        //     }
                        // }
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                        text: ''
                        }
                    },
                    plotOptions: {
                        line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        {
                            name: "total",
                            data: data_total,
                        },
                        {
                            name: "closed",
                            data: data_closed
                        },
                        {
                            name: "cancel",
                            data: data_cancel,
                            color: 'red'
                        },
                        // {
                        //     name: "waiting",
                        //     data: data_waiting
                        // },
                        // {
                        //     name: "on theway",
                        //     data: data_otw
                        // },
                    ]
                });
            } 

        });
    } //getBarReport

    function getBarWaiting(date_from, date_to, terminal, granularity)
    {
        var url_site = "{{url('')}}"+'/api/grafik?date_from='+date_from+'&date_to='+date_to+'&terminal='+terminal+'&granularity='+granularity;   
        $.ajax({
            url   : url_site,
            // url   : 'https://next.json-generator.com/api/json/get/EkqMKs2J9',
            type  : 'GET',
            async : true,
            dataType : 'json',
            success : function(datas){  
                const data_avg = datas.avgs.map((o) => Number(o.avg_waiting_time))
                console.log(data_avg)
                const categories = datas.data.map((o) => String(o.tanggal))
                const data_total = datas.data.map((o) => Number(o.total))
                const data_closed = datas.data.map((o) => Number(o.closed))
                const data_cancel = datas.data.map((o) => Number(o.cancel))
                const data_waiting = datas.data.map((o) => Number(o.waiting))
                const data_otw = datas.data.map((o) => Number(o.otw))
                Highcharts.chart('waiting', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Average Waiting Time'
                    },
                    subtitle: {
                        text: 'total antrian'
                    },
                    xAxis: {
                        categories: categories
                    },
                    yAxis: {
                        title: {
                        text: ''
                        }
                    },
                    plotOptions: {
                        line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
                        {
                            name: "total",
                            data: data_avg
                        },
                        // {
                        //     name: "closed",
                        //     data: data_closed
                        // },
                        // {
                        //     name: "cancel",
                        //     data: data_cancel,
                        //     color: 'red'
                        // },
                        // {
                        //     name: "waiting",
                        //     data: data_waiting
                        // },
                        // {
                        //     name: "on theway",
                        //     data: data_otw
                        // },
                    ]
                });
            } 

        });
    } //getBarWaiting

</script>


@endsection

