<div class="modal fade" id="ubahLokasi" tabindex="-1" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="mediumModalLabel"><strong>Ubah Terminal</strong></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>      
        <div class="modal-body">
            <form action="{{ url('admin/dataLokasi/ubahLokasi') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                
                {{ csrf_field()}}

                <div class="col-lg-12" hidden>
                    <div class="form-group">
                        <label for="id" class=" form-control-label">ID location</label>
                        <input type="text" id="id" name="id" class="form-control" readonly required>
                    </div>
                </div>

                @if(auth()->user()->level_user =='super-admin')
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="id" class=" form-control-label">Bandara</label>
                        <select name="bandara_id" id="bandara_id" class="form-control" required>
                            <option value="">---Pilih Bandara---</option>
                            @foreach($bandara as $b)
                            <option value="{{$b->bandara_id}}">{{$b->bandara_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
                @if(auth()->user()->level_user =='administrator')
                <input type="hidden" name="bandara_id" id="bandara_id" value="{{$bandara->bandara_id}}">
                @endif

                
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="name" class=" form-control-label">Nama Terminal</label>
                        <input type="text" id="name" name="name" placeholder="Masukkan Nama Gerbang" class="form-control" required>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>    
    </div>
</div>
</div>