@extends('Admin/masterAdmin')

@section('judul_tab', 'Lokasi - Admin')
    
@section('active_menu_kelola_lokasi', 'active')

@section('content')

        <div class="breadcrumbs">
            <div class="breadcrumbs-inner">
                <div class="row m-0">
                    <div class="col-sm-4">
                        <div class="page-header float-left">
                            <div class="page-title">
                                <h1>Terminal</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="page-header float-right">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="{{$base_url}}/dashboard">Dashboard</a></li>
                                    <li class="active">Terminal</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">

                    @include('location.create')

                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-header">
                                <strong class="card-title">Kelola Terminal</strong>
                            </div>
                            

                            <div class="card-body">

                                <div class="col-lg-3 col-md-6">
                                    <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahLokasi"><i class="fa fa-plus-square"></i>
                                    Tambah Bandara
                                    </button>
                                </div>
                                </br>
                               

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Bandara</th>
                                            <th>Terminal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($locations as $lok)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$lok->nama_bandara}}</td>
                                            <td>{{$lok->name}}</td>
                                            <td>
                                                <button type="button" class="btn btn-success btn-sm" 
                                                    data-target="#ubahLokasi" 
                                                    data-toggle="modal"
                                                    data-location_id="{{$lok->id}}"
                                                    data-location_name="{{$lok->name}}"
                                                    data-id_bandara="{{$lok->id_bandara}}">
                                                    <i class="fa fa-edit"></i>&nbsp; 
                                                    Ubah
                                                </button>
                                                <a href="{{url('admin/dataLokasi/hapusLokasi').'/'.$lok->id}}" type="button" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>&nbsp;
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal Ubah Lokasi -->
@include('location.edit');

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script> 
    <script src="https://cdn.tiny.cloud/1/cn0rsfqf5862dtcrgnngsfyi4vmj1ketcg7q1gtaw5w115xh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>tinymce.init({selector:'textarea', height: 300});</script>

    <script>
        $('#tgl_rilis').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_rilis2').datetimepicker({
            format: 'dd mmmm yyyy HH:MM' ,
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahLokasi').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var id = button.data('location_id');
              var id_bandara = button.data('id_bandara');
              var location_name = button.data('location_name');

              var modal = $(this);
              modal.find('.modal-body #id').val(id);
              modal.find('.modal-body #bandara_id').val(id_bandara);
              modal.find('.modal-body #name').val(location_name);
            });
        }); 
    </script> 

@endsection