<!-- Modal Tambah Akun-->
<div class="modal fade" id="tambahAkun" role="dialog" aria-labelledby="smallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="mediumModalLabel"><strong>Tambah Petugas</strong></h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>      
                    <div class="modal-body">
                        <form action="{{ url('/admin/dataAkun/tambahAkun') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                            {{ csrf_field()}}

                            

                            <div class="row form-group">
                                <div class="col col-12">
                                    <label><strong>INFORMASI PETUGAS</strong></label>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Username</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="username" name="username" placeholder="Username" class="form-control" required>
                                    <small class="form-text text-muted">Tuliskan username!</small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Password</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="password" id="password" name="password" placeholder="Password" class="form-control" required>
                                    <small class="form-text text-muted">Tuliskan password!</small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Email</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="email" name="email" placeholder="Masukkan Email" class="form-control" required>
                                    <small class="form-text text-muted">Tuliskan email!</small>
                                </div>
                            </div> 

                            {{--
                            <!-- jquery  -->
                            <div class="form-group row">
                                <label for="no_hp" class="col-sm-3 control-label col-form-label" onchange="myFunction()">Bandara</label>
                                <div class="col-sm-9">
                                    <select name="bandara_id" id="bandara_id" class="form-control" required>
                                        <option value="">Pilih Bandara</option>
                                        @foreach($bandara as $b)
                                        <option value="{{ $b->bandara_id }}">{{ $b->bandara_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- ambil dari data device -->
                            <div class="form-group row">
                                <label for="no_hp" class="col-sm-3 control-label col-form-label">Terminal</label>
                                <div class="col-sm-9">
                                    <select name="location_id" id="location_id" class="form-control" required>
                                        <option value="">Pilih Terminal</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <!-- jquery -->
                            --}}

                            <div class="form-group row" hidden>
                                <label for="no_hp" class="col-sm-3 control-label col-form-label">User</label>
                                <div class="col-sm-9">
                                    <input type="text" name="level_user" value="ground staff" id="level_user" class="form-control">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                <label for="text_input" class=" form-control-label">Penempatan</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="level_login" id="level_login" class="form-control" required>
                                        <option value="">---Penempatan Petugas---</option>
                                        <option value="car pool">Car Pool</option>
                                        <option value="pickup point">Pickup Point</option>
                                    </select>
                                </div>
                            </div>
                            
                            @if(auth()->user()->level_user =='super-admin')
                            <!-- jquery  -->
                            <div class="form-group row">
                                <label for="no_hp" class="col-sm-3 control-label col-form-label" onchange="myFunction()">Bandara</label>
                                <div class="col-sm-9">
                                    <select name="bandara_id" id="bandara_id" class="form-control" required>
                                        <option value="">Pilih Bandara</option>
                                        @foreach($bandara as $b)
                                        <option value="{{ $b->bandara_id }}">{{ $b->bandara_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- ambil dari data device -->
                            <div class="form-group row">
                                <label for="no_hp" class="col-sm-3 control-label col-form-label">Terminal</label>
                                <div class="col-sm-9">
                                    <select name="location_id" id="location_id" class="form-control" required>
                                        <option value="">Pilih Terminal</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <!-- jquery -->
                            @endif
                            @if(auth()->user()->level_user =='administrator')
                            <input type="hidden" name="bandara_id" value="{{$bandara->bandara_id}}" id="bandara_id" class="form-control">    
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text_input" class=" form-control-label">Terminal</label></div>
                                <div class="col-12 col-md-9">
                                <select name="location_id" id="location_id" class="form-control" required>
                                    <option value="">---Pilih Terminal---</option>
                                    @foreach($locations as $l)
                                    <option value="{{$l->location_id}}">{{$l->location_name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            @endif 

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Nama Lengkap</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="nama_lengkap" name="nama_lengkap" placeholder="Masukkan Nama Lengkap" class="form-control" required>
                                    <small class="form-text text-muted">Tuliskan nama lengkap!</small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Nomor Kontak</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="no_hp" name="no_hp" placeholder="Masukkan nomor kontak" class="form-control">
                                    <small class="form-text text-muted">Tuliskan nomor kontak!</small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Tempat Lahir</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" class="form-control">
                                    <small class="form-text text-muted">Tuliskan tempat lahir!</small>
                                </div>
                            </div>
                            
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Tanggal Lahir</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="text" id="tgl_lahir" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir" class="form-control" required readonly>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="text-input" class=" form-control-label">Alamat Lengkap</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <textarea id="alamat_rumah" name="alamat_rumah" placeholder="Masukkan Alamat Lengkap" class="form-control" required> </textarea>
                                    <small class="form-text text-muted">Tuliskan alamat lengkap!</small>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text_input" class=" form-control-label">Jenis Kelamin</label></div>
                                <div class="col-12 col-md-9">
                                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                        <option value="">---Pilih Jenis Kelamin---</option>
                                        <option value="laki-laki">Laki-Laki</option>
                                        <option value="perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>            

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>
                    </div>    
                </div>
            </div>
        </div>