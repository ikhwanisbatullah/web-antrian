@extends('Admin/masterAdmin')

@section('judul_tab', 'Akun Pengguna - Admin')
    
@section('active_menu_kelola_akun', 'active')

@section('content')


         <div class="breadcrumbs">
             <div class="breadcrumbs-inner">
                 <div class="row m-0">
                     <div class="col-sm-4">
                         <div class="page-header float-left">
                             <div class="page-title">
                                 <h1>Petugas</h1>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-8">
                         <div class="page-header float-right">
                             <div class="page-title">
                                 <ol class="breadcrumb text-right">
                                     <li><a href="{{$base_url}}/dashboard">Dashboard</a></li>
                                     <li class="active">Petugas</li>
                                 </ol>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>

        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!-- Widgets  -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Kelola Akun Petugas</strong>
                            </div>
                        
                            <div class="card-body">

                                <div class="col-lg-3 col-md-6">
                                    <button type="button" class="btn btn-info mb-1" data-toggle="modal" data-target="#tambahAkun"><i class="fa fa-plus-square"></i>
                                    Tambah Petugas
                                    </button>
                                </div>
                                

                                
                                

                                <br>

                                <table id="bootstrap-data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Username</th>
                                            <!-- <th>Email</th> -->
                                            <th>Bandara</th>
                                            <th>Terminal</th>
                                            <th>Nomor Handphone</th>
                                            <th>Alamat Rumah</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $profile)
                                        <tr>
                                            <td>{{$i+=1}}</td>
                                            <td>{{$profile->username}}</td>
                                            <!-- <td>{{$profile->email}}</td> -->
                                            <td>{{$profile->bandara_name}}</td>
                                            <td>{{$profile->location_name}}</td>
                                            <td>{{$profile->no_hp}}</td>
                                            <td>{{$profile->alamat_rumah}}</td>
                                            <td>
                                            <button type="button" class="btn btn-success btn-sm" 
                                                data-target="#ubahAkun" 
                                                
                                                data-toggle="modal"
                                                data-id_user="{{$profile->user_id}}"
                                                data-nomor_ktp="{{$profile->nomor_ktp}}"
                                                data-nama_lengkap="{{$profile->nama_lengkap}}"
                                                data-jenis_kelamin="{{$profile->jenis_kelamin}}"
                                                data-tempat_lahir="{{$profile->tempat_lahir}}"
                                                data-tgl_lahir="{{$profile->tgl_lahir}}"
                                                data-no_hp="{{$profile->no_hp}}"
                                                data-username="{{$profile->username}}"
                                                data-password="{{$profile->password}}"
                                                data-email="{{$profile->email}}" 
                                            >
                                                <i class="fa fa-edit"></i>&nbsp; 
                                                Ubah
                                            </button>
                                            <a href="{{url('admin/dataAkun/hapusAkun').'/'.$profile->user_id}}" type="button" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>&nbsp;
                                                Hapus
                                            </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Tambah Akun-->
        @include('users.create')

        <!-- Modal Ubah Akun -->
        @include('users.edit')
        
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        // l
        $('#bandara_id').change(function () {
            $('#location_id').empty();
            $('#location_id').append($("<option></option>").attr("value", "").text("Pilih Terminal"));
            $.ajax({
                url: "{{ route('get.terminal') }}",
                type: 'POST',
                data: {id: $("#bandara_id").val()},
                success : function (response) {
                    $.each(response.data, function(key, value){
                    $('#location_id').append($("<option></option>").
                    attr("value", value.location_id).
                    text(value.location_name));
                    });
                }
            });
        });
        $('#edit_bandara_id').change(function () {
            $('#edit_location_id').empty();
            $('#edit_location_id').append($("<option></option>").attr("value", "").text("Pilih Terminal"));
            $.ajax({
                url: "{{ route('get.terminal') }}",
                type: 'POST',
                data: {id: $("#edit_bandara_id").val()},
                success : function (response) {
                    $.each(response.data, function(key, value){
                    $('#edit_location_id').append($("<option></option>").
                    attr("value", value.location_id).
                    text(value.location_name));
                    });
                }
            });
        });
        // l

        $('.js-example-basic-single').select2({
            theme: "classic",
        });
    </script>

    <script>
        $('#tgl_lahir').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
        $('#tgl_lahir2').datepicker({
            format: 'dd mmm yyyy',
            uiLibrary: 'bootstrap4'
        });
    </script>
        
    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    <script language="JavaScript" type="text/JavaScript">
        $('#provinsi2').on('change', function(e){
        console.log(e);
        var prov_id = e.target.value; 

        //ajax

        $.get('/ajax-kota/' + prov_id, function(data){

            $('#kabupaten_kota2').empty();

                $.each(data, function(index, kotaObj){
                    $('#kabupaten_kota2').append('<option value="'+kotaObj.id+'" selected>'+kotaObj.type+" "+kotaObj.nama+'</option>');
                });
            });
        });
    </script>

    
    <script type="text/javascript">
        $(document).ready(function(){
            $('#kabupaten_kota2').on('change', function(){
                $.ajax({
                    url: "/ajax-kodepos",
                    type:"POST",
                    data : {"_token": "{{ csrf_token() }}",
                    "id":$(this).val()},
                    dataType: "json",
                    success: function(res){
                        console.log(res.kodepos);
                        $('#kodepos2').val(res.kodepos);
                    }
                })
              
            })

            // init
            $('#kabupaten_kota2').change();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
              $('#ubahAkun').on('show.bs.modal', function (event) {
              var button = $(event.relatedTarget);
              var id_user = button.data('id_user');
              var nama_lengkap = button.data('nama_lengkap');
              var no_hp =  button.data('no_hp');
              var tempat_lahir =  button.data('tempat_lahir');
              var pend_terakhir = button.data('pend_terakhir');
              var thn_ijazah = button.data('thn_ijazah');
              var tgl_lahir = button.data('tgl_lahir');
              var alamat_lengkap = button.data('alamat_lengkap');
              var provinsi = button.data('provinsi');
              var kabupaten_kota = button.data('kabupaten_kota');
              var kodepos = button.data('kodepos');
              var no_hp = button.data('no_hp');
              
              var ukuran_baju = button.data('ukuran_baju');
              var ukuran_sepatu = button.data('ukuran_sepatu');
              var username = button.data('username');
              var password = button.data('password');
              var email = button.data('email');
             
              var modal = $(this);
              modal.find('.modal-body #id').val(id_user);
              modal.find('.modal-body #nama_lengkap').val(nama_lengkap);
              modal.find('.modal-body #no_hp').val(no_hp);
              modal.find('.modal-body #tgl_lahir2').val(tgl_lahir);
              modal.find('.modal-body #jenis_kelamin').val(jenis_kelamin);
              modal.find('.modal-body #alamat_lengkap').val(alamat_lengkap);
              modal.find('.modal-body #pend_terakhir').val(pend_terakhir);
              modal.find('.modal-body #thn_ijazah').val(thn_ijazah);
              modal.find('.modal-body #provinsi2').val(provinsi);
              modal.find('.modal-body #kabupaten_kota2').val(kabupaten_kota);
              modal.find('.modal-body #kodepos2').val(kodepos);
              modal.find('.modal-body #no_hp').val(no_hp);
              modal.find('.modal-body #ukuran_baju_lain').val(ukuran_baju);
              modal.find('.modal-body #ukuran_sepatu_lain').val(ukuran_sepatu);
              modal.find('.modal-body #username').val(username);
            //   modal.find('.modal-body #password').val(password);
              modal.find('.modal-body #email').val(email);
            });
        }); 
    </script> 



@endsection

