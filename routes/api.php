<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/siswa/{id}/editnilai','ApiController@editnilai');
Route::get('/grafik','API\ApiController@grafik');
Route::get('/dashboard/daily','API\ApiController@dashboard_daily');
Route::get('/dashboard/list','API\ApiController@index');
Route::post('/get/terminal','API\ApiController@getTerminal')->name('get.terminal');


Route::post('register', 'API\UserController@register');
Route::post('login', 'API\UserController@login');
Route::get('book', 'API\BookController@book');


Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('/get/data','API\ApiController@getData');
    Route::get('bookall', 'API\BookController@bookAuth');
    Route::get('user', 'API\UserController@getAuthenticatedUser');
});