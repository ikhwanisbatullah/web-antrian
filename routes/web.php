<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/tests', 'LocationController@tests');
Route::get('/test/{id}', 'LocationController@test');

Route::get('/', 'AuthController@login')->name('login');
Route::get('/logout', 'AuthController@logout');
Route::get('/cek', 'AuthController@cek');
Route::post('/postlogin', 'AuthController@postlogin');

Route::group(['middleware' =>['auth','checkUser:super-admin']],function(){
   // Route::get('/dashboard', 'DashboardController@index');

    Route::get('/admin/dataBandara/kategoriBandara', 'BandaraController@kategori_bandara');
    Route::post('/admin/dataBandara/kategoriBandara/tambahKategoriBandara', 'BandaraController@tambah_kategori_bandara');
    Route::post('/admin/dataBandara/kategoriBandara/ubahKategoriBandara', 'BandaraController@ubah_kategori_bandara');
    Route::get('/admin/dataBandara/kategoriBandara/hapusKategoriBandara/{bandara_id}', 'BandaraController@hapus_kategori_bandara');
    Route::get('/admin/export', 'UsersController@export');

    
    
});


Route::group(['middleware' =>['auth','checkUser:super-admin,administrator,admin-dashboard']],function(){
    Route::get('/dashboard', 'DashboardController@index');
    // Route::get('/dashboard', 'ReportController@chart');
    Route::get('/admin/dataChart/Chart', 'ChartController@index');
    Route::get('/admin/dataChart/Chart1', 'ChartController@cek');
    Route::get('/admin/dataChart/Chart2', 'ChartController@cekk');

    Route::get('/admin/report', 'ReportController@index');
    Route::post('/admin/report', 'ReportController@index');
    // Route::get('ViewPages', 'ViewController@index');
    // Route::post('ViewPages', 'ViewController@index');   
    //route buat administrator khusus buat route utama
    Route::get('/admin/dataLokasi/Lokasi', 'LocationController@lokasi');
    Route::post('/admin/dataLokasi/tambahLokasi', 'LocationController@tambah_lokasi');
    Route::post('/admin/dataLokasi/ubahLokasi', 'LocationController@ubah_lokasi');
    Route::get('/admin/dataLokasi/hapusLokasi/{id}', 'LocationController@hapus_lokasi');

    //route buat administator sub route kelola lokasi
    Route::post('/admin/dataLokasi/ubahLokasiTerminal', 'LocationController@ubah_lokasi_terminal');
    
    Route::get('/admin/dataAkun/akun', 'UsersController@akun');
    Route::post('/admin/dataAkun/tambahAkun', 'UsersController@tambah_akun');
    Route::get('/admin/dataAkun/editAkun/{id}', 'UsersController@edit_akun');
    Route::post('/admin/dataAkun/updateAkun/{id}', 'UsersController@update_akun');
    Route::post('/admin/dataAkun/ubahAkun', 'UsersController@edit');
    Route::get('/admin/dataAkun/hapusAkun/{id}', 'UsersController@hapus_akun');
    Route::get('/admin/chart', 'ReportController@chart');
    Route::get('/admin/chart/export', 'ReportController@export_chart');
});



